package zendeskapi.exception;

public class ZendeskApiException extends RuntimeException {

	private static final long serialVersionUID = 7876550959490761533L;

	public ZendeskApiException(Throwable e) {
		super(e);
	}

	public ZendeskApiException(String message) {
		super(message);
	}

	public ZendeskApiException(String message, Throwable e) {
		super(message, e);
	}

}
