package zendeskapi.extensions;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

public final class JsonHelper {

	public static <T extends Object> T unmarshal(String json, Class<T> objectClass) throws Exception {
		// Unmarshal JSON string to POJO
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		mapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.configure(Feature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper.configure(Feature.WRITE_NULL_MAP_VALUES, false);
		StringReader stringReader = new StringReader(json);
		return mapper.readValue(stringReader, objectClass);
	}
	
	public static <T extends Object> String marshal(T object) throws IOException {
		// Marshal POJO to a JSON string
		StringWriter writer = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		mapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.configure(Feature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper.configure(Feature.WRITE_NULL_MAP_VALUES, false);
		mapper.writeValue(writer, object);
		return writer.toString();
	}
	
	public static String getJsonString(Map<String, String> map) throws IOException  {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(map);
		return json;
	}
}
