package zendeskapi;

public final class StringUtils {
	private StringUtils() {}
	
	public static boolean isNullOrEmpty(String s) {
		if (s == null) {
			return true;
		}
		
		if (s.equals("")) {
			return true;
		}
		
		return false;
	}
}
