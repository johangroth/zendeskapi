package zendeskapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;

import zendeskapi.exception.ZendeskApiException;
import zendeskapi.extensions.JsonHelper;
import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author johan.groth
 * 
 */
public class ZendeskHttpHelper {
	protected String user;
	protected String password;
	protected String zendeskUrl;

	public static enum RequestMethod {
		POST, PUT, GET, DELETE
	};

	/**
	 * Constructor that uses BasicHttpAuthentication.
	 * 
	 * @param zendeskApiUrl
	 * @param user
	 * @param password
	 */
	public ZendeskHttpHelper(String zendeskUrl, String user, String password) {
		this.user = user;
		this.password = password;
		this.zendeskUrl = zendeskUrl;
	}


	protected <T extends ZendeskObject> T genericGet(String resource, Class<T> type) throws Exception {
		return runRequest(resource, RequestMethod.GET, type);
	}

	protected boolean genericDelete(String resource) throws Exception {
		RequestResult res = runRequest(resource, RequestMethod.DELETE, "");
		return res.isOk();
	}

	protected <T, U extends ZendeskObject> U genericPost(String resource, T body, Class<U> returnType) throws Exception {
		return runRequest(resource, RequestMethod.POST, body, returnType);
	}

	protected boolean genericBooleanPost(String resource, String body) throws Exception {
		RequestResult res = runRequest(resource, RequestMethod.POST, body);
		return res.isCreated();
	}

	protected <T, U extends ZendeskObject> U genericPut(String resource, T body, Class<U> returnType) throws Exception {
		return runRequest(resource, RequestMethod.PUT, body, returnType);
	}

	protected boolean genericBooleanPut(String resource, String body) throws Exception {
		RequestResult res = runRequest(resource, RequestMethod.PUT, body);
		return res.isOk();
	}

	protected String getAuthHeader(String userName, String password) {
		String auth = userName + ":" + password;
		auth = new String(Base64.encodeBase64(auth.getBytes()));
		return "Basic " + auth;
	}

	private String getContentAsString(HttpResponse response) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		} catch (IOException e) {
			throw new ZendeskApiException(e);
		}
	}

	public <T extends ZendeskObject> T runRequest(String resource, RequestMethod requestMethod, Class<T> body) throws Exception {
		String payload = JsonHelper.marshal(body);
		RequestResult response = runRequest(resource, requestMethod, payload);

		if (response.isNotCreated() && response.isNotOk()) {
			throw new ZendeskApiException(response.getContent());
		}
		return JsonHelper.unmarshal(response.getContent(), body);
	}

	public <T, U extends ZendeskObject> U runRequest(String resource, RequestMethod requestMethod, T body, Class<U> returnType) throws Exception {
		String payload = JsonHelper.marshal(body);
		RequestResult response = runRequest(resource, requestMethod, payload);

		if (response.isNotCreated() && response.isNotOk()) {
			throw new ZendeskApiException(response.getContent());
		}
		return JsonHelper.unmarshal(response.getContent(), returnType);
	}

	public RequestResult runRequest(String resource, RequestMethod requestMethod, String payload) throws Exception {
		String requestUrl = zendeskUrl;
		if (!requestUrl.endsWith("/")) {
			requestUrl += "/";
		}

		requestUrl += resource;

		HttpResponse response = null;
		RequestResult result = new RequestResult();
		HttpHelper httpHelper = getNewHttpHelper();
		String auth = user + ":" + password;

		switch (requestMethod) {
		case POST:
			response = httpHelper.post(requestUrl, payload, auth);
			break;
		case PUT:
			response = httpHelper.put(requestUrl, payload, auth);
			break;
		case GET:
			response = httpHelper.get(requestUrl, auth);
			break;
		case DELETE:
			response = httpHelper.delete(requestUrl, auth);
			break;
		default:
			break;
		}

		result.setHttpStatusCode(response.getStatusLine().getStatusCode());
		result.setContent(getContentAsString(response));
		return result;
	}

	public HttpHelper getNewHttpHelper() {
		return new HttpHelper();
	}
}
