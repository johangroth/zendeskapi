package zendeskapi;

import zendeskapi.requests.AccountsAndActivity;
import zendeskapi.requests.Attachments;
import zendeskapi.requests.Categories;
import zendeskapi.requests.CustomAgentRoles;
import zendeskapi.requests.Forums;
import zendeskapi.requests.Groups;
import zendeskapi.requests.JobStatuses;
import zendeskapi.requests.Locales;
import zendeskapi.requests.Macros;
import zendeskapi.requests.Organizations;
import zendeskapi.requests.Requests;
import zendeskapi.requests.SatisfactionRatings;
import zendeskapi.requests.Search;
import zendeskapi.requests.SharingAgreements;
import zendeskapi.requests.Tags;
import zendeskapi.requests.Tickets;
import zendeskapi.requests.Topics;
import zendeskapi.requests.Triggers;
import zendeskapi.requests.Users;
import zendeskapi.requests.Views;

public class ZendeskApi {
	private AccountsAndActivity accountsAndActivity;
	private Attachments attachments;
	private Categories categories;
	private CustomAgentRoles customAgentRoles;
	private Forums forums;
	private Groups groups;
	private JobStatuses jobStatuses;
	private Locales locales;
	private Macros macros;
	private Organizations organizations;
	private Requests requests;
	private SatisfactionRatings satisfactionRatings;
	private Search search;
	private SharingAgreements sharingAgreements;
	private Tags tags;
	private Tickets tickets;
	private Topics topics;
	private Triggers triggers;
	private Users users;
	private Views views;

	private String zendeskUrl;

	public ZendeskApi(String yourZendeskUrl, String user, String password) {
		accountsAndActivity = new AccountsAndActivity(yourZendeskUrl, user, password);
		attachments = new Attachments(yourZendeskUrl, user, password);
		categories = new Categories(yourZendeskUrl, user, password);
		customAgentRoles = new CustomAgentRoles(yourZendeskUrl, user, password);
		forums = new Forums(yourZendeskUrl, user, password);
		groups = new Groups(yourZendeskUrl, user, password);
		jobStatuses = new JobStatuses(yourZendeskUrl, user, password);
		locales = new Locales(yourZendeskUrl, user, password);
		macros = new Macros(yourZendeskUrl, user, password);
		organizations = new Organizations(yourZendeskUrl, user, password);
		requests = new Requests(yourZendeskUrl, user, password);
		satisfactionRatings = new SatisfactionRatings(yourZendeskUrl, user, password);
		search = new Search(yourZendeskUrl, user, password);
		sharingAgreements = new SharingAgreements(yourZendeskUrl, user, password);
		tags = new Tags(yourZendeskUrl, user, password);
		tickets = new Tickets(yourZendeskUrl, user, password);
		topics = new Topics(yourZendeskUrl, user, password);
		triggers = new Triggers(yourZendeskUrl, user, password);
		users = new Users(yourZendeskUrl, user, password);
		views = new Views(yourZendeskUrl, user, password);

		zendeskUrl = yourZendeskUrl;
	}

	public AccountsAndActivity getAccountsAndActivity() {
		return accountsAndActivity;
	}

	public void setAccountsAndActivity(AccountsAndActivity accountsAndActivity) {
		this.accountsAndActivity = accountsAndActivity;
	}

	public Attachments getAttachments() {
		return attachments;
	}

	public void setAttachments(Attachments attachments) {
		this.attachments = attachments;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public CustomAgentRoles getCustomAgentRoles() {
		return customAgentRoles;
	}

	public void setCustomAgentRoles(CustomAgentRoles customAgentRoles) {
		this.customAgentRoles = customAgentRoles;
	}

	public Forums getForums() {
		return forums;
	}

	public void setForums(Forums forums) {
		this.forums = forums;
	}

	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}

	public JobStatuses getJobStatuses() {
		return jobStatuses;
	}

	public void setJobStatuses(JobStatuses jobStatuses) {
		this.jobStatuses = jobStatuses;
	}

	public Locales getLocales() {
		return locales;
	}

	public void setLocales(Locales locales) {
		this.locales = locales;
	}

	public Macros getMacros() {
		return macros;
	}

	public void setMacros(Macros macros) {
		this.macros = macros;
	}

	public Organizations getOrganizations() {
		return organizations;
	}

	public void setOrganizations(Organizations organizations) {
		this.organizations = organizations;
	}

	public Requests getRequests() {
		return requests;
	}

	public void setRequests(Requests requests) {
		this.requests = requests;
	}

	public SatisfactionRatings getSatisfactionRatings() {
		return satisfactionRatings;
	}

	public void setSatisfactionRatings(SatisfactionRatings satisfactionRatings) {
		this.satisfactionRatings = satisfactionRatings;
	}

	public Search getSearch() {
		return search;
	}

	public void setSearch(Search search) {
		this.search = search;
	}

	public SharingAgreements getSharingAgreements() {
		return sharingAgreements;
	}

	public void setSharingAgreements(SharingAgreements sharingAgreements) {
		this.sharingAgreements = sharingAgreements;
	}

	public Tags getTags() {
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}

	public Tickets getTickets() {
		return tickets;
	}

	public void setTickets(Tickets tickets) {
		this.tickets = tickets;
	}

	public Topics getTopics() {
		return topics;
	}

	public void setTopics(Topics topics) {
		this.topics = topics;
	}

	public Triggers getTriggers() {
		return triggers;
	}

	public void setTriggers(Triggers triggers) {
		this.triggers = triggers;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Views getViews() {
		return views;
	}

	public void setViews(Views views) {
		this.views = views;
	}

	public String getZendeskUrl() {
		return zendeskUrl;
	}

	public void setZendeskUrl(String zendeskUrl) {
		this.zendeskUrl = zendeskUrl;
	}

}
