package zendeskapi.models.customroles;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;
import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class CustomRoles extends GroupResponseBase implements ZendeskObject {
	@JsonProperty("custom_roles")
	private List<CustomRole> customRoleCollection;

	public List<CustomRole> getCustomRoleCollection() {
		return customRoleCollection;
	}

	public void setCustomRoleCollection(List<CustomRole> customRoleCollection) {
		this.customRoleCollection = customRoleCollection;
	}

}
