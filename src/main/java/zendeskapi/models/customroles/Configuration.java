package zendeskapi.models.customroles;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Configuration {

	@JsonProperty("chat_access")
	private Boolean chatAccess;

	@JsonProperty("end_user_profile")
	private String endUserProfile;

	@JsonProperty("forum_access")
	private String forumAccess;

	@JsonProperty("forum_access_restricted_content")
	private Boolean forumAccessRestrictedContent;

	@JsonProperty("macro_access")
	private String macroAccess;

	@JsonProperty("manage_business_rules")
	private Boolean manageBusinessRules;

	@JsonProperty("manage_dynamic_content")
	private Boolean manageDynamicContent;

	@JsonProperty("manage_extensions_and_channels")
	private Boolean manageExtensionsAndChannels;

	@JsonProperty("manage_facebook")
	private Boolean manageFacebook;

	@JsonProperty("organization_editing")
	private Boolean organizationEditing;

	@JsonProperty("organization_notes_editing")
	private Boolean organizationNotesEditing;

	@JsonProperty("end_user_list_access")
	private String endUserListAccess;
	
	@JsonProperty("report_access")
	private String reportAccess;

	@JsonProperty("ticket_access")
	private String ticketAccess;

	@JsonProperty("ticket_comment_access")
	private String ticketCommentAccess;

	@JsonProperty("voice_access")
	private Boolean voiceAccess;

	@JsonProperty("moderate_forums")
	private Boolean moderateForums;
	
	@JsonProperty("group_access")
	private Boolean groupAccess;
	
	@JsonProperty("end_user_profile_access")
	private String endUserProfileAccess;
	
	@JsonProperty("ticket_deletion")
	private Boolean ticketDeletion;

	@JsonProperty("ticket_editing")
	private Boolean ticketEditing;

	@JsonProperty("ticket_merge")
	private Boolean ticketMerge;

	@JsonProperty("ticket_tag_editing")
	private Boolean ticketTagEditing;

	@JsonProperty("twitter_search_access")
	private Boolean twitterSearchAccess;

	@JsonProperty("view_access")
	private String viewAccess;

	@JsonProperty("user_view_access")
	private String userViewAccess;
	
	public Boolean getChatAccess() {
		return chatAccess;
	}

	public void setChatAccess(Boolean chatAccess) {
		this.chatAccess = chatAccess;
	}

	public String getEndUserProfile() {
		return endUserProfile;
	}

	public void setEndUserProfile(String endUserProfile) {
		this.endUserProfile = endUserProfile;
	}

	public String getForumAccess() {
		return forumAccess;
	}

	public void setForumAccess(String forumAccess) {
		this.forumAccess = forumAccess;
	}

	public Boolean getForumAccessRestrictedContent() {
		return forumAccessRestrictedContent;
	}

	public void setForumAccessRestrictedContent(Boolean forumAccessRestrictedContent) {
		this.forumAccessRestrictedContent = forumAccessRestrictedContent;
	}

	public String getMacroAccess() {
		return macroAccess;
	}

	public void setMacroAccess(String macroAccess) {
		this.macroAccess = macroAccess;
	}

	public Boolean getManageBusinessRules() {
		return manageBusinessRules;
	}

	public void setManageBusinessRules(Boolean manageBusinessRules) {
		this.manageBusinessRules = manageBusinessRules;
	}

	public Boolean getManageDynamicContent() {
		return manageDynamicContent;
	}

	public void setManageDynamicContent(Boolean manageDynamicContent) {
		this.manageDynamicContent = manageDynamicContent;
	}

	public Boolean getManageExtensionsAndChannels() {
		return manageExtensionsAndChannels;
	}

	public void setManageExtensionsAndChannels(Boolean manageExtensionsAndChannels) {
		this.manageExtensionsAndChannels = manageExtensionsAndChannels;
	}

	public Boolean getManageFacebook() {
		return manageFacebook;
	}

	public void setManageFacebook(Boolean manageFacebook) {
		this.manageFacebook = manageFacebook;
	}

	public Boolean getOrganizationEditing() {
		return organizationEditing;
	}

	public void setOrganizationEditing(Boolean organizationEditing) {
		this.organizationEditing = organizationEditing;
	}

	public Boolean getOrganizationNotesEditing() {
		return organizationNotesEditing;
	}

	public void setOrganizationNotesEditing(Boolean organizationNotesEditing) {
		this.organizationNotesEditing = organizationNotesEditing;
	}

	public String getEndUserListAccess() {
		return endUserListAccess;
	}

	public void setEndUserListAccess(String endUserListAccess) {
		this.endUserListAccess = endUserListAccess;
	}

	public String getReportAccess() {
		return reportAccess;
	}

	public void setReportAccess(String reportAccess) {
		this.reportAccess = reportAccess;
	}

	public String getTicketAccess() {
		return ticketAccess;
	}

	public void setTicketAccess(String ticketAccess) {
		this.ticketAccess = ticketAccess;
	}

	public String getTicketCommentAccess() {
		return ticketCommentAccess;
	}

	public void setTicketCommentAccess(String ticketCommentAccess) {
		this.ticketCommentAccess = ticketCommentAccess;
	}

	public Boolean getVoiceAccess() {
		return voiceAccess;
	}

	public void setVoiceAccess(Boolean voiceAccess) {
		this.voiceAccess = voiceAccess;
	}

	public Boolean getModerateForums() {
		return moderateForums;
	}

	public void setModerateForums(Boolean moderateForums) {
		this.moderateForums = moderateForums;
	}

	public Boolean getGroupAccess() {
		return groupAccess;
	}

	public void setGroupAccess(Boolean groupAccess) {
		this.groupAccess = groupAccess;
	}

	public String getEndUserProfileAccess() {
		return endUserProfileAccess;
	}

	public void setEndUserProfileAccess(String endUserProfileAccess) {
		this.endUserProfileAccess = endUserProfileAccess;
	}

	public Boolean getTicketDeletion() {
		return ticketDeletion;
	}

	public void setTicketDeletion(Boolean ticketDeletion) {
		this.ticketDeletion = ticketDeletion;
	}

	public Boolean getTicketEditing() {
		return ticketEditing;
	}

	public void setTicketEditing(Boolean ticketEditing) {
		this.ticketEditing = ticketEditing;
	}

	public Boolean getTicketMerge() {
		return ticketMerge;
	}

	public void setTicketMerge(Boolean ticketMerge) {
		this.ticketMerge = ticketMerge;
	}

	public Boolean getTicketTagEditing() {
		return ticketTagEditing;
	}

	public void setTicketTagEditing(Boolean ticketTagEditing) {
		this.ticketTagEditing = ticketTagEditing;
	}

	public Boolean getTwitterSearchAccess() {
		return twitterSearchAccess;
	}

	public void setTwitterSearchAccess(Boolean twitterSearchAccess) {
		this.twitterSearchAccess = twitterSearchAccess;
	}

	public String getViewAccess() {
		return viewAccess;
	}

	public void setViewAccess(String viewAccess) {
		this.viewAccess = viewAccess;
	}

	public String getUserViewAccess() {
		return userViewAccess;
	}

	public void setUserViewAccess(String userViewAccess) {
		this.userViewAccess = userViewAccess;
	}
	
}
