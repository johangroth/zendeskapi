package zendeskapi.models.macros;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.tickets.Ticket;

/**
 * 
 * @author jgroth
 *
 */
public class Result {
	@JsonProperty("ticket")
	private Ticket ticket;

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	
}
