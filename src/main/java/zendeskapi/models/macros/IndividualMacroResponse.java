package zendeskapi.models.macros;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualMacroResponse implements ZendeskObject {
	@JsonProperty("macro")
	private Macro macro;

	public Macro getMacro() {
		return macro;
	}

	public void setMacro(Macro macro) {
		this.macro = macro;
	}

}
