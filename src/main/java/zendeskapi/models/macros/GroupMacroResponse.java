package zendeskapi.models.macros;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupMacroResponse extends GroupResponseBase {

	@JsonProperty("macros")
	private List<Macro> macros;

	public List<Macro> getMacros() {
		return macros;
	}

	public void setMacros(List<Macro> macros) {
		this.macros = macros;
	}
	
}
