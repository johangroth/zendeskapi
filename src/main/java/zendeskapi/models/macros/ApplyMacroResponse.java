package zendeskapi.models.macros;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

public class ApplyMacroResponse implements ZendeskObject {

	@JsonProperty("result")
	private Result result;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

}
