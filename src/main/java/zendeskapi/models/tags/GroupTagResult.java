package zendeskapi.models.tags;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class GroupTagResult implements ZendeskObject {
	@JsonProperty("count")
	private Integer count;

	@JsonProperty("next_page")
	private Integer nextPage;

	@JsonProperty("previous_page")
	private Integer previousPage;

	@JsonProperty("tags")
	private List<Tag> tags;


	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getNextPage() {
		return nextPage;
	}

	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

	public Integer getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(Integer previousPage) {
		this.previousPage = previousPage;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

}
