package zendeskapi.models.categories;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

public class IndividualCategoryResponse implements ZendeskObject {

	@JsonProperty("category")
	private Category category;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
