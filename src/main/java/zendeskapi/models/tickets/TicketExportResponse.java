package zendeskapi.models.tickets;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class TicketExportResponse implements ZendeskObject {

	@JsonProperty("results")
	private List<TicketExportResult> results;

	@JsonProperty("field_headers")
	private FieldHeaders fieldHeaders;

	@JsonProperty("options")
	private Options options;

	@JsonProperty("end_time")
	private Long endTime;

	@JsonProperty("next_page")
	private String nextPage;

	public List<TicketExportResult> getResults() {
		return results;
	}

	public void setResults(List<TicketExportResult> results) {
		this.results = results;
	}

	public FieldHeaders getFieldHeaders() {
		return fieldHeaders;
	}

	public void setFieldHeaders(FieldHeaders fieldHeaders) {
		this.fieldHeaders = fieldHeaders;
	}

	public Options getOptions() {
		return options;
	}

	public void setOptions(Options options) {
		this.options = options;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getNextPage() {
		return nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}

}
