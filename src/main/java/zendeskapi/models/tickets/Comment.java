package zendeskapi.models.tickets;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.shared.Attachment;

/**
 * 
 * @author jgroth
 * 
 */
public class Comment {

	@JsonProperty("url")
	private String url;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("public")
	private Boolean publicComment;

	@JsonProperty("body")
	private String body;

	@JsonProperty("request_id")
	private Long requestId;

	@JsonProperty("html_body")
	private String htmlBody;

	@JsonProperty("author_id")
	private Long authorId;
	
	@JsonProperty("attachments")
	private List<Attachment> attachments;
	
	@JsonProperty("created_at")
	private Date date;
	
	/**
	 * Used for uploading attachments only. When creating and updating tickets
	 * you may attach files by passing in an array of the tokens received from
	 * uploading the files. For the upload attachment to succeed when updating a
	 * ticket, a comment must be included. Use Attachments.UploadAttachment to
	 * get the token first.
	 * */
	@JsonProperty("uploads")
	private List<String> uploads;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getPublicComment() {
		return publicComment;
	}

	public void setPublicComment(Boolean publicComment) {
		this.publicComment = publicComment;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public String getHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getUploads() {
		return uploads;
	}

	public void setUploads(List<String> uploads) {
		this.uploads = uploads;
	}

}