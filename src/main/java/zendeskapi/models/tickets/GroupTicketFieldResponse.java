package zendeskapi.models.tickets;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupTicketFieldResponse extends GroupResponseBase {
	@JsonProperty("ticket_fields")
	private List<TicketField> ticketFields;

	public List<TicketField> getTicketFields() {
		return ticketFields;
	}

	public void setTicketFields(List<TicketField> ticketFields) {
		this.ticketFields = ticketFields;
	}
	
}
