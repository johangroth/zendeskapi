package zendeskapi.models.tickets;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;
import zendeskapi.models.shared.Audit;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTicketResponse implements ZendeskObject {
	@JsonProperty("ticket")
	private Ticket ticket;

	@JsonProperty("audit")
	private Audit audit;

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

}
