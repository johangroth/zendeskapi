package zendeskapi.models.tickets;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Options {

	@JsonProperty("timezone")
	private String timezone;

	@JsonProperty("hour_offset")
	private String hourOffset;

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getHourOffset() {
		return hourOffset;
	}

	public void setHourOffset(String hourOffset) {
		this.hourOffset = hourOffset;
	}
	
}
