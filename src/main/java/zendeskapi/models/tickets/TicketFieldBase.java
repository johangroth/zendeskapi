package zendeskapi.models.tickets;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class TicketFieldBase {
	@JsonProperty("type")
	private String type;

	@JsonProperty("title")
	private String title;

	@JsonProperty("description")
	private String description;

	@JsonProperty("position")
	private Integer position;

	@JsonProperty("active")
	private Boolean active;

	@JsonProperty("required")
	private Boolean required;

	@JsonProperty("collapsed_for_agents")
	private Boolean collapsedForAgents;

	@JsonProperty("regexp_for_validation")
	private Object regexpForValidation;

	@JsonProperty("title_in_portal")
	private String titleInPortal;

	@JsonProperty("visible_in_portal")
	private Boolean visibleInPortal;

	@JsonProperty("editable_in_portal")
	private Boolean editableInPortal;

	@JsonProperty("required_in_portal")
	private Boolean requiredInPortal;

	@JsonProperty("tag")
	private Object tag;

	@JsonProperty("custom_field_options")
	private List<CustomFieldOptions> customFieldOptions;

	@JsonProperty("removable")
	private Boolean removable;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Boolean getCollapsedForAgents() {
		return collapsedForAgents;
	}

	public void setCollapsedForAgents(Boolean collapsedForAgents) {
		this.collapsedForAgents = collapsedForAgents;
	}

	public Object getRegexpForValidation() {
		return regexpForValidation;
	}

	public void setRegexpForValidation(Object regexpForValidation) {
		this.regexpForValidation = regexpForValidation;
	}

	public String getTitleInPortal() {
		return titleInPortal;
	}

	public void setTitleInPortal(String titleInPortal) {
		this.titleInPortal = titleInPortal;
	}

	public Boolean getVisibleInPortal() {
		return visibleInPortal;
	}

	public void setVisibleInPortal(Boolean visibleInPortal) {
		this.visibleInPortal = visibleInPortal;
	}

	public Boolean getEditableInPortal() {
		return editableInPortal;
	}

	public void setEditableInPortal(Boolean editableInPortal) {
		this.editableInPortal = editableInPortal;
	}

	public Boolean getRequiredInPortal() {
		return requiredInPortal;
	}

	public void setRequiredInPortal(Boolean requiredInPortal) {
		this.requiredInPortal = requiredInPortal;
	}

	public Object getTag() {
		return tag;
	}

	public void setTag(Object tag) {
		this.tag = tag;
	}

	public List<CustomFieldOptions> getCustomFieldOptions() {
		return customFieldOptions;
	}

	public void setCustomFieldOptions(List<CustomFieldOptions> customFieldOptions) {
		this.customFieldOptions = customFieldOptions;
	}

	public Boolean getRemovable() {
		return removable;
	}

	public void setRemovable(Boolean removable) {
		this.removable = removable;
	}

}
