package zendeskapi.models.tickets;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.constants.Priority;
import zendeskapi.models.shared.Via;

/**
 * 
 * @author jgroth
 * 
 */
public class Ticket {
	@JsonProperty("url")
	private String url;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("external_id")
	private Object externalId;

	@JsonProperty("type")
	private String type;

	@JsonProperty("description")
	private String description;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("priority")
	private Priority priority;

	@JsonProperty("status")
	private String status;

	@JsonProperty("recipient")
	private String recipient;

	@JsonProperty("requester_id")
	private Long requesterId;

	@JsonProperty("submitter_id")
	private Long submitterId;

	@JsonProperty("assignee_id")
	private Long assigneeId;

	@JsonProperty("organization_id")
	private Long organizationId;

	@JsonProperty("group_id")
	private Long groupId;

	/**
	 * This is for getting the Ids only
	 */
	@JsonProperty("collaborator_ids")
	private List<Long> collaboratorIds;

	/**
	 * This is used only to update tickets and will not be returned. NOTE that
	 * setting collaborators this way will completely ignore what's already set,
	 * so make sure to include existing collaborators in the array if you wish
	 * to retain these on the ticket.
	 */
	@JsonProperty("collaborators")
	private List<String> collaboratorEmails;

	@JsonProperty("forum_topic_id")
	private Object forumTopicId;

	@JsonProperty("problem_id")
	private Object problemId;

	@JsonProperty("has_incidents")
	private Boolean hasIncidents;

	@JsonProperty("due_at")
	private String dueAt;

	@JsonProperty("tags")
	private List<String> tags;

	@JsonProperty("custom_fields")
	private List<CustomField> customFields;

	@JsonProperty("satisfaction_rating")
	private SatisfactionRating satisfactionRating;

	@JsonProperty("sharing_agreement_ids")
	private List<Long> sharingAgrementIds;

	@JsonProperty("fields")
	private List<CustomField> fields;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("via")
	private Via via;

	// / <summary>
	// / This is used for updates only
	// / </summary>
	@JsonProperty("comment")
	private Comment comment;

	// / <summary>
	// / This is used for updates only
	// / </summary>
	@JsonProperty("requester")
	private Requester requester;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Object getExternalId() {
		return externalId;
	}

	public void setExternalId(Object externalId) {
		this.externalId = externalId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Long getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(Long requesterId) {
		this.requesterId = requesterId;
	}

	public Long getSubmitterId() {
		return submitterId;
	}

	public void setSubmitterId(Long submitterId) {
		this.submitterId = submitterId;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<Long> getCollaboratorIds() {
		return collaboratorIds;
	}

	public void setCollaboratorIds(List<Long> collaboratorIds) {
		this.collaboratorIds = collaboratorIds;
	}

	public List<String> getCollaboratorEmails() {
		return collaboratorEmails;
	}

	public void setCollaboratorEmails(List<String> collaboratorEmails) {
		this.collaboratorEmails = collaboratorEmails;
	}

	public Object getForumTopicId() {
		return forumTopicId;
	}

	public void setForumTopicId(Object forumTopicId) {
		this.forumTopicId = forumTopicId;
	}

	public Object getProblemId() {
		return problemId;
	}

	public void setProblemId(Object problemId) {
		this.problemId = problemId;
	}

	public Boolean getHasIncidents() {
		return hasIncidents;
	}

	public void setHasIncidents(Boolean hasIncidents) {
		this.hasIncidents = hasIncidents;
	}

	public String getDueAt() {
		return dueAt;
	}

	public void setDueAt(String dueAt) {
		this.dueAt = dueAt;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<CustomField> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<CustomField> customFields) {
		this.customFields = customFields;
	}

	public SatisfactionRating getSatisfactionRating() {
		return satisfactionRating;
	}

	public void setSatisfactionRating(SatisfactionRating satisfactionRating) {
		this.satisfactionRating = satisfactionRating;
	}

	public List<Long> getSharingAgrementIds() {
		return sharingAgrementIds;
	}

	public void setSharingAgrementIds(List<Long> sharingAgrementIds) {
		this.sharingAgrementIds = sharingAgrementIds;
	}

	public List<CustomField> getFields() {
		return fields;
	}

	public void setFields(List<CustomField> fields) {
		this.fields = fields;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Requester getRequester() {
		return requester;
	}

	public void setRequester(Requester requester) {
		this.requester = requester;
	}

}
