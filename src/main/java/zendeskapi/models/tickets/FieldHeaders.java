package zendeskapi.models.tickets;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class FieldHeaders {

	@JsonProperty("generated_timestamp")
	private String generatedTimestamp;

	@JsonProperty("req_name")
	private String reqName;

	@JsonProperty("domain")
	private String domain;

	@JsonProperty("submitter_name")
	private String submitterName;

	@JsonProperty("assignee_name")
	private String assigneeName;

	@JsonProperty("group_name")
	private String groupName;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("current_tags")
	private String currentTags;

	@JsonProperty("status")
	private String status;

	@JsonProperty("priority")
	private String priority;

	@JsonProperty("via")
	private String via;

	@JsonProperty("ticket_type")
	private String ticketType;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("assigned_at")
	private String assignedAt;

	@JsonProperty("organization_name")
	private String organizationName;

	@JsonProperty("due_date")
	private String dueDate;

	@JsonProperty("initially_assigned_at")
	private String initiallyAssignedAt;

	@JsonProperty("solved_at")
	private String solvedAt;

	@JsonProperty("resolution_time")
	private String resolutionTime;

	@JsonProperty("satisfaction_score")
	private String satisfactionScore;

	@JsonProperty("group_stations")
	private String groupStations;

	@JsonProperty("assignee_stations")
	private String assigneeStations;

	@JsonProperty("reopens")
	private String reopens;

	@JsonProperty("replies")
	private String replies;

	@JsonProperty("first_reply_time_in_minutes")
	private String firstReplyTimeInMinutes;

	@JsonProperty("first_reply_time_in_minutes_within_business_hours")
	private String firstReplyTimeInMinutesWithinBusinessHours;

	@JsonProperty("first_resolution_time_in_minutes")
	private String firstResolutionTimeInMinutes;

	@JsonProperty("first_resolution_time_in_minutes_within_business_hours")
	private String firstResolutionTimeInMinutesWithinBusinessHours;

	@JsonProperty("full_resolution_time_in_minutes")
	private String fullResolutionTimeInMinutes;

	@JsonProperty("full_resolution_time_in_minutes_within_business_hours")
	private String fullResolutionTimeInMinutesWithinBusinessHours;

	@JsonProperty("agent_wait_time_in_minutes")
	private String agentWaitTimeInMinutes;

	@JsonProperty("agent_wait_time_in_minutes_within_business_hours")
	private String agentWaitTimeInMinutesWithinBusinessHours;

	@JsonProperty("requester_wait_time_in_minutes")
	private String requesterWaitTimeInMinutes;

	@JsonProperty("requester_wait_time_in_minutes_within_business_hours")
	private String requesterWaitTimeInMinutesWithinBusinessHours;

	@JsonProperty("on_hold_time_in_minutes")
	private String onHoldTimeInMinutes;

	@JsonProperty("on_hold_time_in_minutes_within_business_hours")
	private String onHoldTimeInMinutesWithinBusinessHours;

	@JsonProperty("url")
	private String url;

	@JsonProperty("req_external_id")
	private String reqExternalId;

	@JsonProperty("req_email")
	private String reqEmail;

	@JsonProperty("req_id")
	private String reqId;

	@JsonProperty("id")
	private String id;

	public String getGeneratedTimestamp() {
		return generatedTimestamp;
	}

	public void setGeneratedTimestamp(String generatedTimestamp) {
		this.generatedTimestamp = generatedTimestamp;
	}

	public String getReqName() {
		return reqName;
	}

	public void setReqName(String reqName) {
		this.reqName = reqName;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getSubmitterName() {
		return submitterName;
	}

	public void setSubmitterName(String submitterName) {
		this.submitterName = submitterName;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCurrentTags() {
		return currentTags;
	}

	public void setCurrentTags(String currentTags) {
		this.currentTags = currentTags;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getAssignedAt() {
		return assignedAt;
	}

	public void setAssignedAt(String assignedAt) {
		this.assignedAt = assignedAt;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getInitiallyAssignedAt() {
		return initiallyAssignedAt;
	}

	public void setInitiallyAssignedAt(String initiallyAssignedAt) {
		this.initiallyAssignedAt = initiallyAssignedAt;
	}

	public String getSolvedAt() {
		return solvedAt;
	}

	public void setSolvedAt(String solvedAt) {
		this.solvedAt = solvedAt;
	}

	public String getResolutionTime() {
		return resolutionTime;
	}

	public void setResolutionTime(String resolutionTime) {
		this.resolutionTime = resolutionTime;
	}

	public String getSatisfactionScore() {
		return satisfactionScore;
	}

	public void setSatisfactionScore(String satisfactionScore) {
		this.satisfactionScore = satisfactionScore;
	}

	public String getGroupStations() {
		return groupStations;
	}

	public void setGroupStations(String groupStations) {
		this.groupStations = groupStations;
	}

	public String getAssigneeStations() {
		return assigneeStations;
	}

	public void setAssigneeStations(String assigneeStations) {
		this.assigneeStations = assigneeStations;
	}

	public String getReopens() {
		return reopens;
	}

	public void setReopens(String reopens) {
		this.reopens = reopens;
	}

	public String getReplies() {
		return replies;
	}

	public void setReplies(String replies) {
		this.replies = replies;
	}

	public String getFirstReplyTimeInMinutes() {
		return firstReplyTimeInMinutes;
	}

	public void setFirstReplyTimeInMinutes(String firstReplyTimeInMinutes) {
		this.firstReplyTimeInMinutes = firstReplyTimeInMinutes;
	}

	public String getFirstReplyTimeInMinutesWithinBusinessHours() {
		return firstReplyTimeInMinutesWithinBusinessHours;
	}

	public void setFirstReplyTimeInMinutesWithinBusinessHours(
			String firstReplyTimeInMinutesWithinBusinessHours) {
		this.firstReplyTimeInMinutesWithinBusinessHours = firstReplyTimeInMinutesWithinBusinessHours;
	}

	public String getFirstResolutionTimeInMinutes() {
		return firstResolutionTimeInMinutes;
	}

	public void setFirstResolutionTimeInMinutes(String firstResolutionTimeInMinutes) {
		this.firstResolutionTimeInMinutes = firstResolutionTimeInMinutes;
	}

	public String getFirstResolutionTimeInMinutesWithinBusinessHours() {
		return firstResolutionTimeInMinutesWithinBusinessHours;
	}

	public void setFirstResolutionTimeInMinutesWithinBusinessHours(
			String firstResolutionTimeInMinutesWithinBusinessHours) {
		this.firstResolutionTimeInMinutesWithinBusinessHours = firstResolutionTimeInMinutesWithinBusinessHours;
	}

	public String getFullResolutionTimeInMinutes() {
		return fullResolutionTimeInMinutes;
	}

	public void setFullResolutionTimeInMinutes(String fullResolutionTimeInMinutes) {
		this.fullResolutionTimeInMinutes = fullResolutionTimeInMinutes;
	}

	public String getFullResolutionTimeInMinutesWithinBusinessHours() {
		return fullResolutionTimeInMinutesWithinBusinessHours;
	}

	public void setFullResolutionTimeInMinutesWithinBusinessHours(
			String fullResolutionTimeInMinutesWithinBusinessHours) {
		this.fullResolutionTimeInMinutesWithinBusinessHours = fullResolutionTimeInMinutesWithinBusinessHours;
	}

	public String getAgentWaitTimeInMinutes() {
		return agentWaitTimeInMinutes;
	}

	public void setAgentWaitTimeInMinutes(String agentWaitTimeInMinutes) {
		this.agentWaitTimeInMinutes = agentWaitTimeInMinutes;
	}

	public String getAgentWaitTimeInMinutesWithinBusinessHours() {
		return agentWaitTimeInMinutesWithinBusinessHours;
	}

	public void setAgentWaitTimeInMinutesWithinBusinessHours(
			String agentWaitTimeInMinutesWithinBusinessHours) {
		this.agentWaitTimeInMinutesWithinBusinessHours = agentWaitTimeInMinutesWithinBusinessHours;
	}

	public String getRequesterWaitTimeInMinutes() {
		return requesterWaitTimeInMinutes;
	}

	public void setRequesterWaitTimeInMinutes(String requesterWaitTimeInMinutes) {
		this.requesterWaitTimeInMinutes = requesterWaitTimeInMinutes;
	}

	public String getRequesterWaitTimeInMinutesWithinBusinessHours() {
		return requesterWaitTimeInMinutesWithinBusinessHours;
	}

	public void setRequesterWaitTimeInMinutesWithinBusinessHours(
			String requesterWaitTimeInMinutesWithinBusinessHours) {
		this.requesterWaitTimeInMinutesWithinBusinessHours = requesterWaitTimeInMinutesWithinBusinessHours;
	}

	public String getOnHoldTimeInMinutes() {
		return onHoldTimeInMinutes;
	}

	public void setOnHoldTimeInMinutes(String onHoldTimeInMinutes) {
		this.onHoldTimeInMinutes = onHoldTimeInMinutes;
	}

	public String getOnHoldTimeInMinutesWithinBusinessHours() {
		return onHoldTimeInMinutesWithinBusinessHours;
	}

	public void setOnHoldTimeInMinutesWithinBusinessHours(
			String onHoldTimeInMinutesWithinBusinessHours) {
		this.onHoldTimeInMinutesWithinBusinessHours = onHoldTimeInMinutesWithinBusinessHours;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getReqExternalId() {
		return reqExternalId;
	}

	public void setReqExternalId(String reqExternalId) {
		this.reqExternalId = reqExternalId;
	}

	public String getReqEmail() {
		return reqEmail;
	}

	public void setReqEmail(String reqEmail) {
		this.reqEmail = reqEmail;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
