package zendeskapi.models.tickets;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.constants.TicketStatus;

/**
 * 
 * @author jgroth
 * 
 */
public class BulkUpdate {
	@JsonProperty("type")
	private String type;

	@JsonProperty("status")
	private TicketStatus status;

	@JsonProperty("tags")
	private List<String> tags;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("comment")
	private Comment comment;

	@JsonProperty("assignee_id")
	private Long assigneeId;

	// / <summary>
	// / This is used only to update tickets and will not be returned.
	// / NOTE that setting collaborators this way will completely ignore what's
	// already set, so make sure to include existing collaborators in the array
	// if you wish to retain these on the ticket.
	// / </summary>
	@JsonProperty("collaborators")
	private List<String> collaboratorEmails;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public List<String> getCollaboratorEmails() {
		return collaboratorEmails;
	}

	public void setCollaboratorEmails(List<String> collaboratorEmails) {
		this.collaboratorEmails = collaboratorEmails;
	}
	
}
