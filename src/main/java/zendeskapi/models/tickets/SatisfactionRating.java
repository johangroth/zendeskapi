package zendeskapi.models.tickets;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class SatisfactionRating {

	@JsonProperty("score")
	private String score;

	@JsonProperty("comment")
	private String comment;

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
