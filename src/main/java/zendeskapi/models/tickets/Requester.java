package zendeskapi.models.tickets;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Requester {
	// / <summary>
	// / Optional
	// / See ZendeskApi.LocaleValue for more info
	// / </summary>
	@JsonProperty("locale_id")
	private Long localeId;

	// / <summary>
	// / If the email already exists in the system this is optional
	// / </summary>
	@JsonProperty("name")
	private String name;

	// / <summary>
	// /
	// / </summary>
	@JsonProperty("email")
	private String email;

	public Long getLocaleId() {
		return localeId;
	}

	public void setLocaleId(Long localeId) {
		this.localeId = localeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
