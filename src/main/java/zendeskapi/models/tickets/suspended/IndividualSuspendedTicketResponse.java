package zendeskapi.models.tickets.suspended;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualSuspendedTicketResponse implements ZendeskObject {
	@JsonProperty("suspended_ticket")
	private SuspendedTicket suspendedTicket;

	public SuspendedTicket getSuspendedTicket() {
		return suspendedTicket;
	}

	public void setSuspendedTicket(SuspendedTicket suspendedTicket) {
		this.suspendedTicket = suspendedTicket;
	}

}
