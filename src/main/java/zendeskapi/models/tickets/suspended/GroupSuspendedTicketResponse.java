package zendeskapi.models.tickets.suspended;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class GroupSuspendedTicketResponse implements ZendeskObject {

	@JsonProperty("suspended_tickets")
	private List<SuspendedTicket> suspendedTickets;

	@JsonProperty("next_page")
	private Object nextPage;

	@JsonProperty("previous_page")
	private Object previousPage;

	@JsonProperty("count")
	private Integer count;

	public List<SuspendedTicket> getSuspendedTickets() {
		return suspendedTickets;
	}

	public void setSuspendedTickets(List<SuspendedTicket> suspendedTickets) {
		this.suspendedTickets = suspendedTickets;
	}

	public Object getNextPage() {
		return nextPage;
	}

	public void setNextPage(Object nextPage) {
		this.nextPage = nextPage;
	}

	public Object getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(Object previousPage) {
		this.previousPage = previousPage;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
