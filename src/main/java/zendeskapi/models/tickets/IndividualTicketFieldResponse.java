package zendeskapi.models.tickets;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTicketFieldResponse implements ZendeskObject {
	@JsonProperty("ticket_field")
	private TicketField ticketField;

	public TicketField getTicketField() {
		return ticketField;
	}

	public void setTicketField(TicketField ticketField) {
		this.ticketField = ticketField;
	}

}
