package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Apps {

	@JsonProperty("use")
	private Boolean use;

	@JsonProperty("create_private")
	private Boolean createPrivate;

	@JsonProperty("create_public")
	private Boolean createPublic;

	public Boolean getUse() {
		return use;
	}

	public void setUse(Boolean use) {
		this.use = use;
	}

	public Boolean getCreatePrivate() {
		return createPrivate;
	}

	public void setCreatePrivate(Boolean createPrivate) {
		this.createPrivate = createPrivate;
	}

	public Boolean getCreatePublic() {
		return createPublic;
	}

	public void setCreatePublic(Boolean createPublic) {
		this.createPublic = createPublic;
	}
	
}
