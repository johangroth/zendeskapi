package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 * 
 */
public class IndividualActivityResponse implements ZendeskObject {

	@JsonProperty("activity")
	private Activity activity;

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

}
