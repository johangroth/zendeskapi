package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 * 
 */
public class Screencast {

	@JsonProperty("enabled_for_tickets")
	private Boolean enabledForTickets;

	@JsonProperty("host")
	private Object host;

	@JsonProperty("tickets_recorder_id")
	private Object ticketsRecorderId;

	public Boolean getEnabledForTickets() {
		return enabledForTickets;
	}

	public void setEnabledForTickets(Boolean enabledForTickets) {
		this.enabledForTickets = enabledForTickets;
	}

	public Object getHost() {
		return host;
	}

	public void setHost(Object host) {
		this.host = host;
	}

	public Object getTicketsRecorderId() {
		return ticketsRecorderId;
	}

	public void setTicketsRecorderId(Object ticketsRecorderId) {
		this.ticketsRecorderId = ticketsRecorderId;
	}

}
