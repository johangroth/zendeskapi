package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 * 
 */
public class Lotus {

	@JsonProperty("prefer_lotus")
	private Boolean preferLotus;

	@JsonProperty("reporting")
	private Boolean reporting;

	public Boolean getPreferLotus() {
		return preferLotus;
	}

	public void setPreferLotus(Boolean preferLotus) {
		this.preferLotus = preferLotus;
	}

	public Boolean getReporting() {
		return reporting;
	}

	public void setReporting(Boolean reporting) {
		this.reporting = reporting;
	}

}
