package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Settings {

	@JsonProperty("branding")
	private Branding branding;

	@JsonProperty("apps")
	private Apps apps;

	@JsonProperty("tickets")
	private Tickets tickets;

	@JsonProperty("chat")
	private Chat chat;

	@JsonProperty("voice")
	private Voice voice;

	@JsonProperty("twitter")
	private Twitter twitter;

	@JsonProperty("user")
	private User user;

	@JsonProperty("screencast")
	private Screencast screencast;

	@JsonProperty("lotus")
	private Lotus lotus;

	@JsonProperty("gooddata_integration")
	private GoodDataIntegration goodDataIntegration;

	public Branding getBranding() {
		return branding;
	}

	public void setBranding(Branding branding) {
		this.branding = branding;
	}

	public Apps getApps() {
		return apps;
	}

	public void setApps(Apps apps) {
		this.apps = apps;
	}

	public Tickets getTickets() {
		return tickets;
	}

	public void setTickets(Tickets tickets) {
		this.tickets = tickets;
	}

	public Chat getChat() {
		return chat;
	}

	public void setChat(Chat chat) {
		this.chat = chat;
	}

	public Voice getVoice() {
		return voice;
	}

	public void setVoice(Voice voice) {
		this.voice = voice;
	}

	public Twitter getTwitter() {
		return twitter;
	}

	public void setTwitter(Twitter twitter) {
		this.twitter = twitter;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Screencast getScreencast() {
		return screencast;
	}

	public void setScreencast(Screencast screencast) {
		this.screencast = screencast;
	}

	public Lotus getLotus() {
		return lotus;
	}

	public void setLotus(Lotus lotus) {
		this.lotus = lotus;
	}

	public GoodDataIntegration getGoodDataIntegration() {
		return goodDataIntegration;
	}

	public void setGoodDataIntegration(GoodDataIntegration goodDataIntegration) {
		this.goodDataIntegration = goodDataIntegration;
	}
	
}
