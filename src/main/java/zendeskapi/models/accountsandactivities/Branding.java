package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Branding {

	@JsonProperty("header_color")
	private String headerColor;

	@JsonProperty("page_background_color")
	private String pageBackgroundColor;

	@JsonProperty("tab_background_color")
	private String tabBackgroundColor;

	@JsonProperty("text_color")
	private String textColor;

	public String getHeaderColor() {
		return headerColor;
	}

	public void setHeaderColor(String headerColor) {
		this.headerColor = headerColor;
	}

	public String getPageBackgroundColor() {
		return pageBackgroundColor;
	}

	public void setPageBackgroundColor(String pageBackgroundColor) {
		this.pageBackgroundColor = pageBackgroundColor;
	}

	public String getTabBackgroundColor() {
		return tabBackgroundColor;
	}

	public void setTabBackgroundColor(String tabBackgroundColor) {
		this.tabBackgroundColor = tabBackgroundColor;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	
}
