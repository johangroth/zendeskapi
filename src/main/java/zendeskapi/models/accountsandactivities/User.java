package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class User {

	@JsonProperty("tagging")
	private Boolean tagging;

	public Boolean getTagging() {
		return tagging;
	}

	public void setTagging(Boolean tagging) {
		this.tagging = tagging;
	}
	
}
