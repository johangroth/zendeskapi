package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Tickets {

	@JsonProperty("list_newest_comments_first")
	private Boolean listNewestCommentsFirst;

	@JsonProperty("collaboration")
	private Boolean collaboration;

	@JsonProperty("private_attachments")
	private Boolean privateAttachments;

	@JsonProperty("agent_collision")
	private Boolean agentCollision;

	@JsonProperty("tagging")
	private Boolean tagging;

	@JsonProperty("list_empty_views")
	private Boolean listEmptyViews;

	@JsonProperty("comments_public_by_default")
	private Boolean commentsPublicByDefault;

	@JsonProperty("maximum_personal_views_to_list")
	private Integer maximumPersonalViewsToList;

	@JsonProperty("status_hold")
	private Boolean statusHold;

	public Boolean getListNewestCommentsFirst() {
		return listNewestCommentsFirst;
	}

	public void setListNewestCommentsFirst(Boolean listNewestCommentsFirst) {
		this.listNewestCommentsFirst = listNewestCommentsFirst;
	}

	public Boolean getCollaboration() {
		return collaboration;
	}

	public void setCollaboration(Boolean collaboration) {
		this.collaboration = collaboration;
	}

	public Boolean getPrivateAttachments() {
		return privateAttachments;
	}

	public void setPrivateAttachments(Boolean privateAttachments) {
		this.privateAttachments = privateAttachments;
	}

	public Boolean getAgentCollision() {
		return agentCollision;
	}

	public void setAgentCollision(Boolean agentCollision) {
		this.agentCollision = agentCollision;
	}

	public Boolean getTagging() {
		return tagging;
	}

	public void setTagging(Boolean tagging) {
		this.tagging = tagging;
	}

	public Boolean getListEmptyViews() {
		return listEmptyViews;
	}

	public void setListEmptyViews(Boolean listEmptyViews) {
		this.listEmptyViews = listEmptyViews;
	}

	public Boolean getCommentsPublicByDefault() {
		return commentsPublicByDefault;
	}

	public void setCommentsPublicByDefault(Boolean commentsPublicByDefault) {
		this.commentsPublicByDefault = commentsPublicByDefault;
	}

	public Integer getMaximumPersonalViewsToList() {
		return maximumPersonalViewsToList;
	}

	public void setMaximumPersonalViewsToList(Integer maximumPersonalViewsToList) {
		this.maximumPersonalViewsToList = maximumPersonalViewsToList;
	}

	public Boolean getStatusHold() {
		return statusHold;
	}

	public void setStatusHold(Boolean statusHold) {
		this.statusHold = statusHold;
	}
	
}
