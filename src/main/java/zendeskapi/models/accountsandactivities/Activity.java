package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 * 
 */
public class Activity {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("url")
	private String url;

	@JsonProperty("verb")
	private String verb;

	@JsonProperty("title")
	private String title;

	@JsonProperty("user")
	private User user;

	@JsonProperty("actor")
	private User actor;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;
}
