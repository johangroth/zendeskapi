package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**    
 * 
 * @author jgroth
 *
 */
public class Chat {

	@JsonProperty("enabled")
	private Boolean enabled;

	@JsonProperty("maximum_request_count")
	private Object maximumRequestCount;

	@JsonProperty("welcome_message")
	private String welcomeMessage;

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Object getMaximumRequestCount() {
		return maximumRequestCount;
	}

	public void setMaximumRequestCount(Object maximumRequestCount) {
		this.maximumRequestCount = maximumRequestCount;
	}

	public String getWelcomeMessage() {
		return welcomeMessage;
	}

	public void setWelcomeMessage(String welcomeMessage) {
		this.welcomeMessage = welcomeMessage;
	}
	
}
