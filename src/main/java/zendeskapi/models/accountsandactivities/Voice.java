package zendeskapi.models.accountsandactivities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Voice {

	@JsonProperty("enabled")
	private Boolean enabled;

	@JsonProperty("maintenance")
	private Boolean maintenance;

	@JsonProperty("logging")
	private Boolean logging;

	@JsonProperty("outbound")
	private Boolean outbound;

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getMaintenance() {
		return maintenance;
	}

	public void setMaintenance(Boolean maintenance) {
		this.maintenance = maintenance;
	}

	public Boolean getLogging() {
		return logging;
	}

	public void setLogging(Boolean logging) {
		this.logging = logging;
	}

	public Boolean getOutbound() {
		return outbound;
	}

	public void setOutbound(Boolean outbound) {
		this.outbound = outbound;
	}
	
}
