package zendeskapi.models.users;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualUserIdentityResponse implements ZendeskObject {

	@JsonProperty("identity")
	private UserIdentity identity;

	public UserIdentity getIdentity() {
		return identity;
	}

	public void setIdentity(UserIdentity identity) {
		this.identity = identity;
	}

}
