package zendeskapi.models.users;

import org.codehaus.jackson.annotate.JsonProperty;

public class Password {

	@JsonProperty("password")
	private String password;
	
	@JsonProperty("previous_password")
	private String previousPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPreviousPassword() {
		return previousPassword;
	}

	public void setPreviousPassword(String previousPassword) {
		this.previousPassword = previousPassword;
	}
	
}
