package zendeskapi.models.users;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.shared.Attachment;

/**
 * 
 * @author jgroth
 *
 */
public class User {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("url")
	private String url;

	@JsonProperty("name")
	private String name;

	@JsonProperty("external_id")
	private String externalId;

	@JsonProperty("alias")
	private String alias;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("active")
	private Boolean active;

	@JsonProperty("verified")
	private Boolean verified;

	@JsonProperty("shared")
	private Boolean shared;

	@JsonProperty("shared_agent")
	private Boolean sharedAgent;

	@JsonProperty("locale")
	private String locale;

	@JsonProperty("locale_id")
	private Long localeId;

	@JsonProperty("time_zone")
	private String timeZone;

	@JsonProperty("last_login_at")
	private String lastLoginAt;

	@JsonProperty("email")
	private String email;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("signature")
	private String signature;

	@JsonProperty("details")
	private String details;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("organization_id")
	private Long organizationId;

	@JsonProperty("role")
	private String role;

	@JsonProperty("custom_role_id")
	private Long customRoleId;

	@JsonProperty("moderator")
	private Boolean moderator;

	@JsonProperty("ticket_restriction")
	private String ticketRestriction;

	@JsonProperty("only_private_comments")
	private Boolean onlyPrivateComments;

	@JsonProperty("tags")
	private List<String> tags;

	@JsonProperty("suspended")
	private Boolean suspended;;

	@JsonProperty("authenticity_token")
	private String authenticityToken;

	@JsonProperty("restricted_agent")
	private Boolean restrictedAgent;

	@JsonProperty("photo")
	private Attachment photo;

	@JsonProperty("user_fields")
	private Map<String, String> userFields;

	@JsonProperty("agent")
	private Boolean agent;

	public String getAuthenticityToken() {
		return authenticityToken;
	}

	public void setAuthenticityToken(String authenticityToken) {
		this.authenticityToken = authenticityToken;
	}

	public Boolean getSharedAgent() {
		return sharedAgent;
	}

	public void setSharedAgent(Boolean sharedAgent) {
		this.sharedAgent = sharedAgent;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Boolean getRestrictedAgent() {
		return restrictedAgent;
	}

	public void setRestrictedAgent(Boolean restrictedAgent) {
		this.restrictedAgent = restrictedAgent;
	}

	public Map<String, String> getUserFields() {
		return userFields;
	}

	public void setUserFields(Map<String, String> userFields) {
		this.userFields = userFields;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public Boolean getShared() {
		return shared;
	}

	public void setShared(Boolean shared) {
		this.shared = shared;
	}

	public Long getLocaleId() {
		return localeId;
	}

	public void setLocaleId(Long localeId) {
		this.localeId = localeId;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(String lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Long getCustomRoleId() {
		return customRoleId;
	}

	public void setCustomRoleId(Long customRoleId) {
		this.customRoleId = customRoleId;
	}

	public Boolean getModerator() {
		return moderator;
	}

	public void setModerator(Boolean moderator) {
		this.moderator = moderator;
	}

	public String getTicketRestriction() {
		return ticketRestriction;
	}

	public void setTicketRestriction(String ticketRestriction) {
		this.ticketRestriction = ticketRestriction;
	}

	public Boolean getOnlyPrivateComments() {
		return onlyPrivateComments;
	}

	public void setOnlyPrivateComments(Boolean onlyPrivateComments) {
		this.onlyPrivateComments = onlyPrivateComments;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Boolean getSuspended() {
		return suspended;
	}

	public void setSuspended(Boolean suspended) {
		this.suspended = suspended;
	}

	public Attachment getPhoto() {
		return photo;
	}

	public void setPhoto(Attachment photo) {
		this.photo = photo;
	}

	public Boolean getAgent() {
		return agent;
	}

	public void setAgent(Boolean agent) {
		this.agent = agent;
	}
}
