package zendeskapi.models.users;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Zendesk allows admins to customise fields displayed on a User profile page. 
 * Basic text fields, date fields, as well as customisable dropdown and number fields are available. 
 * These fields are currently only visible to agents.
 * 
 * @author jgroth
 *
 */
public class UserField {
	/**
	 * Automatically assigned upon creation
	 * Read-only: yes
	 * Mandatory: no
	 * 
	 */
	@JsonProperty("id")
	private Integer id; 

	/**
	 * The URL for this resource
	 * Read-only: yes
	 * Mandatory: no
	 */
	@JsonProperty("url")
	private String url; 	
	
	/**
	 * A unique key that identifies this custom field. 
	 * This is used for updating the field and referencing in placeholders.
	 * Read-only: no 
	 * Mandatory: on create
	 */
	@JsonProperty("key")
	private String key;  	

	/**
	 * Supported types: "text", "textarea", "checkbox", "date", "integer", "decimal", "regexp", 
	 * "tagger" (custom dropdown)
	 * Read-only: no
	 * Mandatory: yes
	 */
	@JsonProperty("type")
	private String type; 	
	
	/**
	 * The title of the custom field
	 * Read-only: no
	 * Mandatory: yes
	 */
	@JsonProperty("title")
	private String title; 	
	
	/**
	 * User-defined description of this field's purpose
	 * Read-only: no
	 * Mandatory: no
	 */
	@JsonProperty("description")
	private String description; 	
	
	/**
	 * Ordering of the field relative to other fields
	 * Read-only: no 
	 * Mandatory: no
	 */
	@JsonProperty("position")
	private Integer position; 	
	
	/**
	 * If true, this field is available for use
	 * Read-only: no
	 * Mandatory: no
	 */
	@JsonProperty("active")
	private Boolean active; 	
	
	/**
	 * Regular expression field only. The validation pattern for a field value to be deemed valid.
	 * Read-only: no
	 * Mandatory: no
	 */
	@JsonProperty("regexp_for_validation")
	private String regexpForValidation;  	
	
	/**
	 * The time the ticket field was created
	 * Read-only: no
	 * Mandatory: no
	 */
	@JsonProperty("created_at")
	private Date createdAt; 	
	
	/**
	 * The time of the last update of the ticket field
	 * Read-only: yes
	 * Mandatory: no
	 */
	@JsonProperty("updated_at")
	private Date updatedAt; 	
	
	/**
	 * Optional for custom field of type "checkbox"; not presented otherwise.
	 * Read-only: no
	 * Mandatory: no
	 */
	@JsonProperty
	private String tag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getRegexpForValidation() {
		return regexpForValidation;
	}

	public void setRegexpForValidation(String regexpForValidation) {
		this.regexpForValidation = regexpForValidation;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
}
