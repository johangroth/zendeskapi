package zendeskapi.models.users;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class GroupUserFieldResponse {

	@JsonProperty("user_fields")
	private List<UserField> userFields;

	public List<UserField> getUserFields() {
		return userFields;
	}

	public void setUserFields(List<UserField> userFields) {
		this.userFields = userFields;
	}
}
