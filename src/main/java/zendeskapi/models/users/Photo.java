package zendeskapi.models.users;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.shared.Thumbnail;

/**
 * 
 * @author jgroth
 * 
 */
public class Photo {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("content_url")
	private String contentUrl;

	@JsonProperty("content_type")
	private String contentType;

	@JsonProperty("size")
	private Integer size;

	@JsonProperty("thumbnails")
	private List<Thumbnail> thumbnails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public List<Thumbnail> getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(List<Thumbnail> thumbnails) {
		this.thumbnails = thumbnails;
	}
	
}
