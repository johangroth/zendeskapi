package zendeskapi.models.users;

import org.codehaus.jackson.annotate.JsonProperty;

public class IndividualUserFieldResponse {

	@JsonProperty("user_field")
	private UserField userField;

	public UserField getUserField() {
		return userField;
	}

	public void setUserField(UserField userField) {
		this.userField = userField;
	}
	
	
}
