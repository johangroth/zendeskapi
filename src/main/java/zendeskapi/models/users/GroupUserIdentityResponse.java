package zendeskapi.models.users;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupUserIdentityResponse extends GroupResponseBase {

	@JsonProperty("identities")
	private List<UserIdentity> identities;

	public List<UserIdentity> getIdentities() {
		return identities;
	}

	public void setIdentities(List<UserIdentity> identities) {
		this.identities = identities;
	}
	
}
