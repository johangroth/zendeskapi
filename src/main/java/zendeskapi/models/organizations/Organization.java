package zendeskapi.models.organizations;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Organization {

	@JsonProperty("url")
	private String url;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("external_id")
	private Long externalId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("domain_names")
	private List<Object> domainNames;

	@JsonProperty("details")
	private Object details;

	@JsonProperty("notes")
	private Object notes;

	@JsonProperty("group_id")
	private Object groupId;

	@JsonProperty("shared_tickets")
	private Boolean sharedTickets;

	@JsonProperty("shared_comments")
	private Boolean sharedComments;

	@JsonProperty("tags")
	private List<Object> tags;

	@JsonProperty("organization_fields")
	private Map<String, String> organizationFields;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Object> getDomainNames() {
		return domainNames;
	}

	public void setDomainNames(List<Object> domainNames) {
		this.domainNames = domainNames;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public Object getNotes() {
		return notes;
	}

	public void setNotes(Object notes) {
		this.notes = notes;
	}

	public Object getGroupId() {
		return groupId;
	}

	public void setGroupId(Object groupId) {
		this.groupId = groupId;
	}

	public Boolean getSharedTickets() {
		return sharedTickets;
	}

	public void setSharedTickets(Boolean sharedTickets) {
		this.sharedTickets = sharedTickets;
	}

	public Boolean getSharedComments() {
		return sharedComments;
	}

	public void setSharedComments(Boolean sharedComments) {
		this.sharedComments = sharedComments;
	}

	public List<Object> getTags() {
		return tags;
	}

	public void setTags(List<Object> tags) {
		this.tags = tags;
	}

	public Map<String, String> getOrganizationFields() {
		return organizationFields;
	}

	public void setOrganizationFields(Map<String, String> organizationFields) {
		this.organizationFields = organizationFields;
	}
	
}
