package zendeskapi.models.forums;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupForumSubcriptionResponse extends GroupResponseBase {

	@JsonProperty("forum_subscriptions")
	private List<ForumSubscription> forumSubscriptions;

	public List<ForumSubscription> getForumSubscriptions() {
		return forumSubscriptions;
	}

	public void setForumSubscriptions(List<ForumSubscription> forumSubscriptions) {
		this.forumSubscriptions = forumSubscriptions;
	}
	
}
