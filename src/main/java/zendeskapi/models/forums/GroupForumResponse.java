package zendeskapi.models.forums;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupForumResponse extends GroupResponseBase {
	@JsonProperty("forums")
	private List<Forum> forums;

	public List<Forum> getForums() {
		return forums;
	}

	public void setForums(List<Forum> forums) {
		this.forums = forums;
	}
	
}
