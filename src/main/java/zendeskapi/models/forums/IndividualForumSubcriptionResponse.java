package zendeskapi.models.forums;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualForumSubcriptionResponse implements ZendeskObject {
	@JsonProperty("forum_subscription")
	private ForumSubscription forumSubscription;

	public ForumSubscription getForumSubscription() {
		return forumSubscription;
	}

	public void setForumSubscription(ForumSubscription forumSubscription) {
		this.forumSubscription = forumSubscription;
	}

}
