package zendeskapi.models.groups;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class GroupMembership {

	@JsonProperty("url")
	private String url;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("user_id")
	private Long userId;

	@JsonProperty("group_id")
	private Long groupId;

	@JsonProperty("default")
	private Boolean defaultGroup;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Boolean getDefaultGroup() {
		return defaultGroup;
	}

	public void setDefaultGroup(Boolean defaultGroup) {
		this.defaultGroup = defaultGroup;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
