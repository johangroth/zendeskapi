package zendeskapi.models.groups;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualGroupResponse implements ZendeskObject {
	@JsonProperty("group")
	private Group group;

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
