package zendeskapi.models.groups;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualGroupMembershipResponse implements ZendeskObject {
	@JsonProperty("group_membership")
	private GroupMembership groupMembership;

	public GroupMembership getGroupMembership() {
		return groupMembership;
	}

	public void setGroupMembership(GroupMembership groupMembership) {
		this.groupMembership = groupMembership;
	}

}
