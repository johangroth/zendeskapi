package zendeskapi.models.groups;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class MultipleGroupMembershipResponse extends GroupResponseBase {

	@JsonProperty("group_memberships")
	private List<GroupMembership> groupMemberships;

	public List<GroupMembership> getGroupMemberships() {
		return groupMemberships;
	}

	public void setGroupMemberships(List<GroupMembership> groupMemberships) {
		this.groupMemberships = groupMemberships;
	}
	
}
