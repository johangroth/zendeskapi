package zendeskapi.models.satisfactionratings;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupSatisfactionResponse extends GroupResponseBase {

	@JsonProperty("satisfaction_ratings")
	private List<SatisfactionRating> satisfactionRatings;

	public List<SatisfactionRating> getSatisfactionRatings() {
		return satisfactionRatings;
	}

	public void setSatisfactionRatings(List<SatisfactionRating> satisfactionRatings) {
		this.satisfactionRatings = satisfactionRatings;
	}
	
}
