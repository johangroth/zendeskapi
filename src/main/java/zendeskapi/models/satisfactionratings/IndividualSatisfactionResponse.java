package zendeskapi.models.satisfactionratings;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualSatisfactionResponse implements ZendeskObject {

	@JsonProperty("satisfaction_rating")
	private SatisfactionRating satisfactionRating;

	public SatisfactionRating getSatisfactionRating() {
		return satisfactionRating;
	}

	public void setSatisfactionRating(SatisfactionRating satisfactionRating) {
		this.satisfactionRating = satisfactionRating;
	}

}
