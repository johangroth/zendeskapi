package zendeskapi.models.views;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

public class GroupViewCountResponse implements ZendeskObject {
	@JsonProperty("view_counts")
	private List<ViewCount> viewCounts;

	public List<ViewCount> getViewCounts() {
		return viewCounts;
	}

	public void setViewCounts(List<ViewCount> viewCounts) {
		this.viewCounts = viewCounts;
	}

}
