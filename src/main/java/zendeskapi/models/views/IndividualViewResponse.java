package zendeskapi.models.views;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualViewResponse implements ZendeskObject {
	@JsonProperty("view")
	private View view;

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

}
