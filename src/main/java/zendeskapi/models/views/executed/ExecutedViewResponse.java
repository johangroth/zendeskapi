package zendeskapi.models.views.executed;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class ExecutedViewResponse extends GroupResponseBase {

	@JsonProperty("rows")
	private List<Row> rows;

	@JsonProperty("columns")
	private List<Column> columns;

	@JsonProperty("view")
	private View view;

	@JsonProperty("users")
	private List<ExecutedUser> users;

	public List<Row> getRows() {
		return rows;
	}

	public void setRows(List<Row> rows) {
		this.rows = rows;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public List<ExecutedUser> getUsers() {
		return users;
	}

	public void setUsers(List<ExecutedUser> users) {
		this.users = users;
	}
	
}
