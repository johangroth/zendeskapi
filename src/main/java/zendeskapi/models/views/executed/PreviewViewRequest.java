package zendeskapi.models.views.executed;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class PreviewViewRequest {
	@JsonProperty("view")
	private PreviewView view;

	@JsonProperty("group_by")
	private String groupBy;

	@JsonProperty("group_order")
	private String groupOrder;

	@JsonProperty("sort_order")
	private String sortOrder;

	@JsonProperty("sort_by")
	private String sortBy;

	public PreviewView getView() {
		return view;
	}

	public void setView(PreviewView view) {
		this.view = view;
	}

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	public String getGroupOrder() {
		return groupOrder;
	}

	public void setGroupOrder(String groupOrder) {
		this.groupOrder = groupOrder;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
}
