package zendeskapi.models.views.executed;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Row {

	@JsonProperty("custom_fields")
	private List<CustomField> customFields;

	@JsonProperty("score")
	private Integer score;

	@JsonProperty("subject")
	private String subject;

	@JsonProperty("requester_id")
	private Long requesterId;

	@JsonProperty("created")
	private String created;

	@JsonProperty("priority")
	private String priority;

	@JsonProperty("updated")
	private String updated;

	@JsonProperty("assignee_id")
	private Long assigneeId;

	@JsonProperty("fields")
	private List<Field> fields;

	@JsonProperty("ticket")
	private ExecutedTicket executedTicket;

	public List<CustomField> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<CustomField> customFields) {
		this.customFields = customFields;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Long getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(Long requesterId) {
		this.requesterId = requesterId;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public ExecutedTicket getExecutedTicket() {
		return executedTicket;
	}

	public void setExecutedTicket(ExecutedTicket executedTicket) {
		this.executedTicket = executedTicket;
	}
	
}
