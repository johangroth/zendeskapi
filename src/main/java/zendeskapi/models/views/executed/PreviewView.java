package zendeskapi.models.views.executed;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.views.All;

/**
 * 
 * @author jgroth
 *
 */
public class PreviewView {

	@JsonProperty("all")
	private List<All> all;

	@JsonProperty("output")
	private PreviewViewOutput output;

	public List<All> getAll() {
		return all;
	}

	public void setAll(List<All> all) {
		this.all = all;
	}

	public PreviewViewOutput getOutput() {
		return output;
	}

	public void setOutput(PreviewViewOutput output) {
		this.output = output;
	}
	
}
