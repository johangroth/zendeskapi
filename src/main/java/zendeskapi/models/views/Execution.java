package zendeskapi.models.views;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Execution {

	@JsonProperty("group_by")
	private String groupBy;

	@JsonProperty("group_order")
	private String groupOrder;

	@JsonProperty("sort_by")
	private String sortBy;

	@JsonProperty("sort_order")
	private String sortOrder;

	@JsonProperty("fields")
	private List<Field> fields;

	@JsonProperty("custom_fields")
	private List<Object> customFields;

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	public String getGroupOrder() {
		return groupOrder;
	}

	public void setGroupOrder(String groupOrder) {
		this.groupOrder = groupOrder;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public List<Object> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<Object> customFields) {
		this.customFields = customFields;
	}
	
}
