package zendeskapi.models.views;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualViewCountResponse implements ZendeskObject {
	@JsonProperty("view_count")
	private ViewCount viewCount;

	public ViewCount getViewCount() {
		return viewCount;
	}

	public void setViewCount(ViewCount viewCount) {
		this.viewCount = viewCount;
	}

}
