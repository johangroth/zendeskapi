package zendeskapi.models.views;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Conditions {

	@JsonProperty("all")
	private List<All> all;

	@JsonProperty("any")
	private List<Object> any;

	public List<All> getAll() {
		return all;
	}

	public void setAll(List<All> all) {
		this.all = all;
	}

	public List<Object> getAny() {
		return any;
	}

	public void setAny(List<Object> any) {
		this.any = any;
	}
	
}
