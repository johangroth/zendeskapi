package zendeskapi.models.views;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class ViewCount {

	@JsonProperty("view_id")
	private Long viewId;

	@JsonProperty("url")
	private String url;

	@JsonProperty("value")
	private Long value;

	@JsonProperty("pretty")
	private String pretty;

	@JsonProperty("fresh")
	private Boolean fresh;

	public Long getViewId() {
		return viewId;
	}

	public void setViewId(Long viewId) {
		this.viewId = viewId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getPretty() {
		return pretty;
	}

	public void setPretty(String pretty) {
		this.pretty = pretty;
	}

	public Boolean getFresh() {
		return fresh;
	}

	public void setFresh(Boolean fresh) {
		this.fresh = fresh;
	}
	
}
