package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum ViaTypes {
	WEB("web"),
	EMAIL("email"),
	API("api"),
	RULE("rule"),
	FORUM("forum"),
	TWITTER("twitter"),
	CHAT("chat"),
	VOICE("voice"),
	SMS("sms"),
	FACEBOOK("facebook"),
	SYSTEM("system");
	
	private ViaTypes(String value) {
		this.value = value;
	}
	
	public String value;
}
