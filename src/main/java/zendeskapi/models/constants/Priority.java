package zendeskapi.models.constants;

/**
 * jdsaflkas
 * @author jgroth
 * 
 */
public enum Priority {
	URGENT("urgent"),
	urgent("urgent"),
	HIGH("high"),
	high("high"),
	NORMAL("normal"),
	normal("normal"),
	LOW("low"),
	low("low");
	
	public String priority;
	
	private Priority(String priority) {
		this.priority = priority;
	}
}
