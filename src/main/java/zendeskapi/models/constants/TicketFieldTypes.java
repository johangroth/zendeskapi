package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum TicketFieldTypes {
	TEXT("text"),
	TEXT_AREA("textarea"),
	CHECKBOX("checkbox"),
	DATE("date"),
	INTEGER("integer"),
	DECIMAL("decimal"),
	REGEXP("regexp"),
	TAGGER("tagger");
	
	private TicketFieldTypes(String value) {
		this.value = value;
	}
	
	public String value;
}

