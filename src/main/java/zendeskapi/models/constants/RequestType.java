package zendeskapi.models.constants;

public enum RequestType {
	QUESTION("QUESTION"),
	question("question"),
	INCIDENT("INCIDENT"),
	incident("incident"),
	PROBLEM("PROBLEM"),
	problem("problem"),
	TASK("TASK"),
	task("task");
	
	public String type;
	
	private RequestType(String type) {
		this.type = type;
	}
}
