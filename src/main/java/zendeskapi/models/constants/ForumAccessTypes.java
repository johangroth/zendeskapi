package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum ForumAccessTypes {
	EVERY_BODY("everybody"),
	LOGGED_IN_USERS("logged-in users"),
	AGENTS_ONLY("agents only");
	
	private ForumAccessTypes(String value) {
		this.value = value;
	}
	
	public String value;
}
