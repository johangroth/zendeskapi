package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum AuditTypes {
	COMMENT("Comment"),
	VOICE_COMMENT("VoiceComment"),
	COMMENT_PRIVACY_CHANGE("CommentPrivacyChange"),
	CREATE("CreateTicket"),
	CHANGE("Change"),
	NOTIFICATION("Notification"),
	CC("Cc"),
	ERROR("Error"),
	EXTERNAL("External"),
	FACEBOOK_EVENT("FacebookEvent"),
	LOG_ME_IN_TRANSCRIPT("LogMeInTranscript"),
	PUSH("Push"),
	SATISFACTION_RATING("SatisfactionRating"),
	TWEET("Tweet"),
	SMS("SMS"),
	TICKET_SHARING_EVENT("TicketSharingEvent");
	
	AuditTypes(String value) {
		this.value = value;
	}
	
	public String value;
}
