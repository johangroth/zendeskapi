package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum TicketStatus {
	NEW("new"),
	OPEN("open"),
	open("open"),
	PENDING("pending"),
	pending("pending"),
	HOLD("hold"),
	hold("hold"),
	SOLVED("solved"),
	solved("solved"),
	CLOSED("closed"),
	closed("closed");

	private TicketStatus(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}
}
