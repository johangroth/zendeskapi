package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum UserIdentityTypes {
	EMAIL("email"),
	TWITTER("twitter"),
	FACEBOOK("facebook"),
	GOOGLE("google");
	
	private UserIdentityTypes(String value) {
		this.value = value;
	}
	public String value;
}
