package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum ForumTypes {
	ARTICLES("articles"),
	IDEAS("ideas"),
	QUESTIONS("questions");
	
	private ForumTypes(String value) {
		this.value = value;
	}
	
	public String value;
}
