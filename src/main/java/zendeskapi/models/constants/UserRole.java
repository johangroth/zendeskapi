package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum UserRole {
	END_USER("end-user"),
	end_user("end-user"),
	AGENT("agent"),
	agent("agent"),
	ADMIN("admin"),
	admin("admin");
	
	UserRole(String value) {
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}
}
