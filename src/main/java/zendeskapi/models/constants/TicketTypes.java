package zendeskapi.models.constants;

/**
 * 
 * @author jgroth
 *
 */
public enum TicketTypes {
	PROBLEM("problem"),
	INCIDENT("incident"),
	QUESTION("question"),
	TASK("task");
	
	private TicketTypes(String value) {
		this.value = value;
	}
	public String value;
}
