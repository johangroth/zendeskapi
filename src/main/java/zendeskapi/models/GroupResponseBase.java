package zendeskapi.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 * 
 */
public class GroupResponseBase implements ZendeskObject {

	@JsonProperty("next_page")
	private String nextPage;

	@JsonProperty("previous_page")
	private String previousPage;

	@JsonProperty("count")
	private Integer count;

	public String getNextPage() {
		return nextPage;
	}

	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}

	public String getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
