package zendeskapi.models.requests;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;
import zendeskapi.models.organizations.Organization;
import zendeskapi.models.tickets.Comment;
import zendeskapi.models.users.User;

/**
 * 
 * @author jgroth
 *
 */
public class GroupCommentResponse extends GroupResponseBase {
	@JsonProperty("comments")
	private List<Comment> comments;

	@JsonProperty("users")
	private List<User> users;
	
	@JsonProperty("organizations")
	private List<Organization> organizations;
	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Organization> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<Organization> organizations) {
		this.organizations = organizations;
	}
	
}
