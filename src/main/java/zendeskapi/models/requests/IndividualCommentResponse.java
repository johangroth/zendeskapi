package zendeskapi.models.requests;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;
import zendeskapi.models.organizations.Organization;
import zendeskapi.models.tickets.Comment;
import zendeskapi.models.users.User;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualCommentResponse implements ZendeskObject {
	@JsonProperty("comment")
	private Comment comment;

	@JsonProperty("users")
	private List<User> users;

	@JsonProperty("organizations")
	private List<Organization> organization;

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Organization> getOrganization() {
		return organization;
	}

	public void setOrganization(List<Organization> organization) {
		this.organization = organization;
	}

}
