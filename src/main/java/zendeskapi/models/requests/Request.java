package zendeskapi.models.requests;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.constants.Priority;
import zendeskapi.models.constants.RequestType;
import zendeskapi.models.shared.Via;
import zendeskapi.models.tickets.Comment;
import zendeskapi.models.tickets.CustomField;

/**
 * 
 * @author jgroth
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

	@JsonProperty("url")
	private String url;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("status")
	private String status;

	@JsonProperty("priority")
	private Priority priority;
	
	@JsonProperty("type")
	private RequestType type;
	
	@JsonProperty("subject")
	private String subject;

	@JsonProperty("description")
	private String description;

	@JsonProperty("organization_id")
	private Long organizationId;

	@JsonProperty("via")
	private Via via;

	@JsonProperty("custom_fields")
	private List<CustomField> customFields;

	@JsonProperty("requester_id")
	private Long requesterId;
	
	@JsonProperty("collaborator_ids")
	private List<Long> collaboratorIds;
	
	@JsonProperty("due_at")
	private Date dueAt;
	
	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("assignee_id")
	private Long assigneeId;
	
	@JsonProperty("fields")
	private List<String> fields;
	
	/**
	 * This is used for updates only
	 */
	@JsonProperty("comment")
	private Comment comment;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public RequestType getType() {
		return type;
	}

	public void setType(RequestType type) {
		this.type = type;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

	public List<CustomField> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<CustomField> customFields) {
		this.customFields = customFields;
	}

	public Long getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(Long requesterId) {
		this.requesterId = requesterId;
	}

	public List<Long> getCollaboratorIds() {
		return collaboratorIds;
	}

	public void setCollaboratorIds(List<Long> collaboratorIds) {
		this.collaboratorIds = collaboratorIds;
	}

	public Date getDueAt() {
		return dueAt;
	}

	public void setDueAt(Date dueAt) {
		this.dueAt = dueAt;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
}
