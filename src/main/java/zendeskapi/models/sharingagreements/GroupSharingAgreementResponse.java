package zendeskapi.models.sharingagreements;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupSharingAgreementResponse extends GroupResponseBase {

	@JsonProperty("sharing_agreements")
	private List<SharingAgreement> sharingAgreements;

	public List<SharingAgreement> getSharingAgreements() {
		return sharingAgreements;
	}

	public void setSharingAgreements(List<SharingAgreement> sharingAgreements) {
		this.sharingAgreements = sharingAgreements;
	}
	
}
