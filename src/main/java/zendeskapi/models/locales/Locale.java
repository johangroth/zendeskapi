package zendeskapi.models.locales;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Locale {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("url")
	private String url;

	@JsonProperty("locale")
	private String localeValue;

	@JsonProperty("default")
	private Boolean defaultLocale;
	
	@JsonProperty("name")
	private String name;

	@JsonProperty("rtl")
	private Boolean rtl;
	
	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	public Boolean getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(Boolean defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public Boolean getRtl() {
		return rtl;
	}

	public void setRtl(Boolean rtl) {
		this.rtl = rtl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLocaleValue() {
		return localeValue;
	}

	public void setLocaleValue(String localeValue) {
		this.localeValue = localeValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
