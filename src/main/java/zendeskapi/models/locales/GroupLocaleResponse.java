package zendeskapi.models.locales;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupLocaleResponse extends GroupResponseBase {

	@JsonProperty("locales")
	private List<Locale> locales;

	public List<Locale> getLocales() {
		return locales;
	}

	public void setLocales(List<Locale> locales) {
		this.locales = locales;
	}
	
}
