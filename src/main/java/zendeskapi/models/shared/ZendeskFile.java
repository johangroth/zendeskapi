package zendeskapi.models.shared;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * 
 * @author jgroth
 * 
 */
public class ZendeskFile {
	private String fileName;
	private String contentType;
	private byte[] fileData;

	public ZendeskFile(String fileName) {
		this.fileName = fileName;
		fileData = readFileData();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	private byte[] readFileData() {
		try {
			return Files.readAllBytes(Paths.get(fileName));
		} catch (IOException e) {
			return null;
		}
	}
}
