package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class UploadResult {
	@JsonProperty("upload")
	private Upload upload;

	public Upload getUpload() {
		return upload;
	}

	public void setUpload(Upload upload) {
		this.upload = upload;
	}
	
}
