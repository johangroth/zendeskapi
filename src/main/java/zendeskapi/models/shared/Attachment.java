package zendeskapi.models.shared;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 * id 	integer 	yes 	Automatically assigned when created
file_name 	string 	yes 	The name of the image file
content_url 	string 	yes 	A full URL where the attachment image file can be downloaded
content_type 	string 	yes 	The content type of the image. Example value: image/png
size 	integer 	yes 	The size of the image file in bytes
thumbnails 	array 	yes 	An array of Photo objects. Note that thumbnails do not have thumbnails.
 *
 */
public class Attachment {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("url")
	private String url;

	@JsonProperty("file_name")
	private String fileName;

	@JsonProperty("content_url")
	private String contentUrl;

	@JsonProperty("mapped_content_url")
	private String mappedContentUrl;

	@JsonProperty("content_type")
	private String contentType;

	@JsonProperty("size")
	private Integer size;

	@JsonProperty("thumbnails")
	private List<Thumbnail> thumbnails;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	public String getMappedContentUrl() {
		return mappedContentUrl;
	}

	public void setMappedContentUrl(String mappedContentUrl) {
		this.mappedContentUrl = mappedContentUrl;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public List<Thumbnail> getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(List<Thumbnail> thumbnails) {
		this.thumbnails = thumbnails;
	}

}
