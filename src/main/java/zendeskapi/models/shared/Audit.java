package zendeskapi.models.shared;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.tickets.suspended.Via;

/**
 * 
 * @author jgroth
 *
 */
public class Audit {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("ticket_id")
	private String ticketId;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("author_id")
	private Long authorId;

	@JsonProperty("metadata")
	private MetaData metaData;

	@JsonProperty("via")
	private Via via;

	@JsonProperty("events")
	private List<Event> events;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
}
