package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class JobStatusResponse implements ZendeskObject {
	@JsonProperty("job_status")
	private JobStatus jobStatus;

	public JobStatus getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(JobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

}
