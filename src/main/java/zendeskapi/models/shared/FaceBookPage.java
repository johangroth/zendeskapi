package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class FaceBookPage {
	// / <summary>
	// / Used when event is FacebookEvent
	// / </summary>
	@JsonProperty("name")
	private String name;

	// / <summary>
	// / Used when event is FacebookEvent
	// / </summary>
	@JsonProperty("graph_id")
	private String graphId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGraphId() {
		return graphId;
	}

	public void setGraphId(String graphId) {
		this.graphId = graphId;
	}
	
}
