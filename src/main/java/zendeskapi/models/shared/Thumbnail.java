package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 * 
 */
public class Thumbnail {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("file_name")
	private String fileName;

	@JsonProperty("content_url")
	private String contentUrl;

	@JsonProperty("content_type")
	private String contentType;

	@JsonProperty("size")
	private Integer size;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
}
