package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Result {

	@JsonProperty("title")
	private String title;

	@JsonProperty("action")
	private String action;

	@JsonProperty("errors")
	private String errors;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("success")
	private Boolean success;

	@JsonProperty("status")
	private String status;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
