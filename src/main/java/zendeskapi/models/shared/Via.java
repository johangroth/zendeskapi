package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.tickets.Source;

/**
 * 
 * @author jgroth
 *
 */
public class Via {
	@JsonProperty("channel")
	private String channel;

	@JsonProperty("source")
	private Source source;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}
	
}
