package zendeskapi.models.shared;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.tickets.Custom;

/**
 * 
 * @author jgroth
 *
 */
public class MetaData {
	@JsonProperty("custom")
	private Custom custom;

	@JsonProperty("system")
	private System system;

	public Custom getCustom() {
		return custom;
	}

	public void setCustom(Custom custom) {
		this.custom = custom;
	}

	public System getSystem() {
		return system;
	}

	public void setSystem(System system) {
		this.system = system;
	}
	
}
