package zendeskapi.models.shared;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.tickets.suspended.Via;

/**
 * 
 * @author jgroth
 *
 */
public class Event {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("type")
	private String type;

	@JsonProperty("body")
	private String body;

	@JsonProperty("public")
	private Boolean publicEvent;

	@JsonProperty("attachments")
	private List<Attachment> attachments;

	// / <summary>
	// / Used when event is Comment
	// / </summary>
	@JsonProperty("author_id")
	private Long authorId;

	// / <summary>
	// / Used when event is Comment
	// / </summary>
	@JsonProperty("html_body")
	private String htmlBody;

	// / <summary>
	// / Used when event is Comment
	// / </summary>
	@JsonProperty("trusted")
	private Boolean trusted;

	// / <summary>
	// / Used when event is VoiceComment
	// / </summary>
	@JsonProperty("data")
	private String data;

	// / <summary>
	// / Used when event is VoiceComment
	// / </summary>
	@JsonProperty("formatted_from")
	private String formattedFrom;

	// / <summary>
	// / Used when event is VoiceComment
	// / </summary>
	@JsonProperty("transcription_visible")
	private String transcriptionVisible;

	// / <summary>
	// / Used when event is CommentPrivacyChange
	// / </summary>
	@JsonProperty("comment_id")
	private Long commentId;

	// / <summary>
	// / Used when event is CreateTicket or Change
	// / </summary>
	@JsonProperty("field_name")
	private String fieldName;

	// / <summary>
	// / Used when event is CreateTicket or Change
	// / </summary>
	@JsonProperty("value")
	private Object value;

	// / <summary>
	// / Used when event is Change
	// / </summary>
	@JsonProperty("previous_value")
	private Object previousValue;

	// / <summary>
	// / Used when event is Notification
	// / </summary>
	@JsonProperty("subject")
	private String subject;

	// / <summary>
	// / Used when event is Notification
	// / </summary>
	@JsonProperty("via")
	private Via via;

	// / <summary>
	// / Used when event is Notification
	// / </summary>
	@JsonProperty("recipients")
	private List<String> recipients;

	// / <summary>
	// / Used when event is Error
	// / </summary>
	@JsonProperty("message")
	private String message;

	// / <summary>
	// / Used when event is External
	// / </summary>
	@JsonProperty("success")
	private String success;

	// / <summary>
	// / Used when event is External
	// / </summary>
	@JsonProperty("resource")
	private String resource;

	// / <summary>
	// / Used when event is FacebookEvent
	// / </summary>
	@JsonProperty("communication")
	private Long communication;

	// / <summary>
	// / Used when event is FacebookEvent
	// / </summary>
	@JsonProperty("ticket_via")
	private String ticketVia;

	// / <summary>
	// / Used when event is FacebookEvent
	// / </summary>
	@JsonProperty("page")
	private FaceBookPage facebookPage;

	// / <summary>
	// / Used when event is Push
	// / </summary>
	@JsonProperty("value_reference")
	private String valueReference;

	// / <summary>
	// / Used when event is SatisfactionRating
	// / </summary>
	@JsonProperty("assignee_id")
	private String assigneeId;

	// / <summary>
	// / Used when event is SatisfactionRating
	// / </summary>
	@JsonProperty("score")
	private String score;

	// / <summary>
	// / Used when event is Tweet
	// / </summary>
	@JsonProperty("direct_message")
	private String directMessage;

	// / <summary>
	// / Used when event is SMS
	// / </summary>
	@JsonProperty("phone_number")
	private String phoneNumber;

	// / <summary>
	// / Used when event is SMS
	// / </summary>
	@JsonProperty("recipient_id")
	private String recipientId;

	// / <summary>
	// / Used when event is TicketSharingEvent
	// / </summary>
	@JsonProperty("agreement_id")
	private String agreementId;

	// / <summary>
	// / Used when event is TicketSharingEvent
	// / </summary>
	@JsonProperty("action")
	private String action;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Boolean getPublicEvent() {
		return publicEvent;
	}

	public void setPublicEvent(Boolean publicEvent) {
		this.publicEvent = publicEvent;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public Boolean getTrusted() {
		return trusted;
	}

	public void setTrusted(Boolean trusted) {
		this.trusted = trusted;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getFormattedFrom() {
		return formattedFrom;
	}

	public void setFormattedFrom(String formattedFrom) {
		this.formattedFrom = formattedFrom;
	}

	public String getTranscriptionVisible() {
		return transcriptionVisible;
	}

	public void setTranscriptionVisible(String transcriptionVisible) {
		this.transcriptionVisible = transcriptionVisible;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getPreviousValue() {
		return previousValue;
	}

	public void setPreviousValue(Object previousValue) {
		this.previousValue = previousValue;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public Long getCommunication() {
		return communication;
	}

	public void setCommunication(Long communication) {
		this.communication = communication;
	}

	public String getTicketVia() {
		return ticketVia;
	}

	public void setTicketVia(String ticketVia) {
		this.ticketVia = ticketVia;
	}

	public FaceBookPage getFacebookPage() {
		return facebookPage;
	}

	public void setFacebookPage(FaceBookPage facebookPage) {
		this.facebookPage = facebookPage;
	}

	public String getValueReference() {
		return valueReference;
	}

	public void setValueReference(String valueReference) {
		this.valueReference = valueReference;
	}

	public String getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getDirectMessage() {
		return directMessage;
	}

	public void setDirectMessage(String directMessage) {
		this.directMessage = directMessage;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public String getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
}
