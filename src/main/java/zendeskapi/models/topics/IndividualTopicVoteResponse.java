package zendeskapi.models.topics;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTopicVoteResponse implements ZendeskObject {
	@JsonProperty("topic_vote")
	private TopicVote topicVote;

	public TopicVote getTopicVote() {
		return topicVote;
	}

	public void setTopicVote(TopicVote topicVote) {
		this.topicVote = topicVote;
	}

}
