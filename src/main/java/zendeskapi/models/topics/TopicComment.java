package zendeskapi.models.topics;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.shared.Attachment;

/**
 * 
 * @author jgroth
 *
 */
public class TopicComment {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("topic_id")
	private Long topicId;

	@JsonProperty("user_id")
	private Long userId;

	@JsonProperty("body")
	private String body;

	@JsonProperty("informative")
	private Boolean informative;

	@JsonProperty("attachments")
	private List<Attachment> attachments;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	// / <summary>
	// / Used for uploading attachments only
	// / When creating and updating comments you may attach files by passing in
	// an array of the tokens received from uploading the files.
	// / Use Attachments.UploadAttachment to get the token first.
	// / </summary>
	@JsonProperty("uploads")
	private List<String> uploads;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Boolean getInformative() {
		return informative;
	}

	public void setInformative(Boolean informative) {
		this.informative = informative;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<String> getUploads() {
		return uploads;
	}

	public void setUploads(List<String> uploads) {
		this.uploads = uploads;
	}

}
