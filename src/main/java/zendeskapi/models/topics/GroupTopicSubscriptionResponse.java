package zendeskapi.models.topics;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupTopicSubscriptionResponse extends GroupResponseBase {
	@JsonProperty("topic_subscriptions")
	private List<TopicSubscription> topicSubscriptions;

	public List<TopicSubscription> getTopicSubscriptions() {
		return topicSubscriptions;
	}

	public void setTopicSubscriptions(List<TopicSubscription> topicSubscriptions) {
		this.topicSubscriptions = topicSubscriptions;
	}
	
}
