package zendeskapi.models.topics;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTopicSubscriptionResponse implements ZendeskObject {
	@JsonProperty("topic_subscription")
	private TopicSubscription topicSubscription;

	public TopicSubscription getTopicSubscription() {
		return topicSubscription;
	}

	public void setTopicSubscription(TopicSubscription topicSubscription) {
		this.topicSubscription = topicSubscription;
	}

}
