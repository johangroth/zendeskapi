package zendeskapi.models.topics;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTopicResponse implements ZendeskObject {
	@JsonProperty("topic")
	private Topic topic;

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

}
