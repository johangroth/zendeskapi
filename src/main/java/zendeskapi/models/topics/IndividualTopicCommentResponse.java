package zendeskapi.models.topics;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTopicCommentResponse implements ZendeskObject {
	@JsonProperty("topic_comment")
	private TopicComment topicComment;

	public TopicComment getTopicComment() {
		return topicComment;
	}

	public void setTopicComment(TopicComment topicComment) {
		this.topicComment = topicComment;
	}

}
