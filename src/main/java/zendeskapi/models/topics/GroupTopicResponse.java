package zendeskapi.models.topics;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupTopicResponse extends GroupResponseBase {

	@JsonProperty("topics")
	private List<Topic> topics;

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	
}
