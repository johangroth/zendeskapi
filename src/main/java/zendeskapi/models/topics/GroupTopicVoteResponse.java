package zendeskapi.models.topics;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupTopicVoteResponse extends GroupResponseBase {

	@JsonProperty("topic_votes")
	private List<TopicVote> topicVotes;

	public List<TopicVote> getTopicVotes() {
		return topicVotes;
	}

	public void setTopicVotes(List<TopicVote> topicVotes) {
		this.topicVotes = topicVotes;
	}
	
}
