package zendeskapi.models.topics;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class TopicVote {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("user_id")
	private Long userId;

	@JsonProperty("topic_id")
	private Long topicId;

	@JsonProperty("created_at")
	private String createdAt;
}
