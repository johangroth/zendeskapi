package zendeskapi.models.topics;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.GroupResponseBase;

/**
 * 
 * @author jgroth
 *
 */
public class GroupTopicCommentResponse extends GroupResponseBase {

	@JsonProperty("topic_comments")
	private List<TopicComment> topicComments;

	public List<TopicComment> getTopicComments() {
		return topicComments;
	}

	public void setTopicComments(List<TopicComment> topicComments) {
		this.topicComments = topicComments;
	}
	
}
