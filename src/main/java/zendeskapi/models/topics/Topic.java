package zendeskapi.models.topics;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.shared.Attachment;

/**
 * 
 * @author jgroth
 *
 */
public class Topic {

	@JsonProperty("url")
	private String url;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("title")
	private String title;

	@JsonProperty("body")
	private String body;

	@JsonProperty("topic_type")
	private String topicType;

	@JsonProperty("submitter_id")
	private Long submitterId;

	@JsonProperty("updater_id")
	private Long updaterId;

	@JsonProperty("forum_id")
	private Long forumId;

	@JsonProperty("locked")
	private Boolean locked;

	@JsonProperty("pinned")
	private Boolean pinned;

	@JsonProperty("highlighted")
	private Boolean highlighted;

	@JsonProperty("position")
	private Integer position;

	@JsonProperty("tags")
	private List<String> tags;

	@JsonProperty("attachments")
	private List<Attachment> attachments;

	@JsonProperty("comments_count")
	private Integer commentsCount;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("updated_at")
	private String updatedAt;

	// / <summary>
	// / Used for uploading Topics only
	// / When creating and updating Topics you may attach files by passing in an
	// array of the tokens received from uploading the files.
	// / Use Attachments.UploadAttachment to get the token first.
	// / </summary>
	@JsonProperty("uploads")
	private List<String> uploads;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTopicType() {
		return topicType;
	}

	public void setTopicType(String topicType) {
		this.topicType = topicType;
	}

	public Long getSubmitterId() {
		return submitterId;
	}

	public void setSubmitterId(Long submitterId) {
		this.submitterId = submitterId;
	}

	public Long getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(Long updaterId) {
		this.updaterId = updaterId;
	}

	public Long getForumId() {
		return forumId;
	}

	public void setForumId(Long forumId) {
		this.forumId = forumId;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Boolean getPinned() {
		return pinned;
	}

	public void setPinned(Boolean pinned) {
		this.pinned = pinned;
	}

	public Boolean getHighlighted() {
		return highlighted;
	}

	public void setHighlighted(Boolean highlighted) {
		this.highlighted = highlighted;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<String> getUploads() {
		return uploads;
	}

	public void setUploads(List<String> uploads) {
		this.uploads = uploads;
	}

}
