package zendeskapi.models.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jgroth
 *
 */
public class Conditions {

	@JsonProperty("all")
	private List<All> all;

	@JsonProperty("any")
	private List<All> any;

	public List<All> getAll() {
		return all;
	}

	public void setAll(List<All> all) {
		this.all = all;
	}

	public List<All> getAny() {
		return any;
	}

	public void setAny(List<All> any) {
		this.any = any;
	}
	
}
