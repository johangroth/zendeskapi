package zendeskapi.models.triggers;

import org.codehaus.jackson.annotate.JsonProperty;

import zendeskapi.models.ZendeskObject;

/**
 * 
 * @author jgroth
 *
 */
public class IndividualTriggerResponse implements ZendeskObject {
	@JsonProperty("trigger")
	private Trigger trigger;

	public Trigger getTrigger() {
		return trigger;
	}

	public void setTrigger(Trigger trigger) {
		this.trigger = trigger;
	}

}
