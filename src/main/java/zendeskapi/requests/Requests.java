package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.requests.GroupCommentResponse;
import zendeskapi.models.requests.GroupRequestResponse;
import zendeskapi.models.requests.IndividualCommentResponse;
import zendeskapi.models.requests.IndividualRequestResponse;
import zendeskapi.models.requests.Request;
import zendeskapi.models.tickets.Comment;

public class Requests extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Requests(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupRequestResponse getAllRequests() throws ZendeskApiException {
		try {
			return genericGet("requests.json", GroupRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupRequestResponse getAllSolvedRequests() throws ZendeskApiException {
		try {
			return genericGet("requests/solved.json", GroupRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupRequestResponse getAllCcdRequests() throws ZendeskApiException {
		try {
			return genericGet("requests/ccd.json", GroupRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupRequestResponse getAllRequestsForUser(long id) throws ZendeskApiException {
		try {
			return genericGet("users/" + id + "/requests.json", GroupRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualRequestResponse getRequestById(long id) throws ZendeskApiException {
		try {
			return genericGet("requests/" + id + ".json", IndividualRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupCommentResponse getRequestCommentsById(long id) throws ZendeskApiException {
		try {
			return genericGet("requests/" + id + "/comments.json", GroupCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param requestId
	 * @param commentId
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualCommentResponse getSpecificRequestComment(long requestId, long commentId) throws ZendeskApiException {
		try {
			return genericGet("requests/" + requestId + "/comments/" + commentId + ".json", IndividualCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param request
	 * @return IndividualRequestResponse
	 * @throws ZendeskApiException
	 */
	public IndividualRequestResponse createRequest(Request request) throws ZendeskApiException {
		IndividualRequestResponse individualRequestResponse = new IndividualRequestResponse();
		individualRequestResponse.setRequest(request);
		try {
			return genericPost("requests.json", individualRequestResponse, IndividualRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of request " + request.getSubject() + " failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @param comment
	 * @return IndividualRequestResponse
	 * @throws ZendeskApiException
	 */
	public IndividualRequestResponse updateRequest(long id, Comment comment) throws ZendeskApiException {
		Request request = new Request();
		request.setComment(comment);
		IndividualRequestResponse individualRequestResponse = new IndividualRequestResponse();
		individualRequestResponse.setRequest(request);
		try {
			return genericPut("requests/" + id + ".json", individualRequestResponse, IndividualRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of request " + request.getSubject() + " failed", e);
		}
	}

	/**
	 * 
	 * @param request
	 * @param comment
	 * @return IndividualRequestResponse
	 * @throws ZendeskApiException
	 */
	public IndividualRequestResponse updateRequest(Request request) throws ZendeskApiException {
		IndividualRequestResponse individualRequestResponse = new IndividualRequestResponse();
		individualRequestResponse.setRequest(request);
		try {
			return genericPut("requests/" + request.getId() + ".json", individualRequestResponse, IndividualRequestResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of request " + request.getSubject() + " failed", e);
		}
	}
}
