package zendeskapi.requests;

import java.util.List;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.topics.GroupTopicCommentResponse;
import zendeskapi.models.topics.GroupTopicResponse;
import zendeskapi.models.topics.GroupTopicSubscriptionResponse;
import zendeskapi.models.topics.GroupTopicVoteResponse;
import zendeskapi.models.topics.IndividualTopicCommentResponse;
import zendeskapi.models.topics.IndividualTopicResponse;
import zendeskapi.models.topics.IndividualTopicSubscriptionResponse;
import zendeskapi.models.topics.IndividualTopicVoteResponse;
import zendeskapi.models.topics.Topic;
import zendeskapi.models.topics.TopicComment;
import zendeskapi.models.topics.TopicSubscription;
import zendeskapi.models.topics.TopicVote;

public class Topics extends ZendeskHttpHelper {
	public Topics(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	public GroupTopicResponse getTopics() throws ZendeskApiException {
		try {
			return genericGet("topics.json", GroupTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicResponse getTopicById(long topicId) throws ZendeskApiException {
		try {
			return genericGet("topics/" + topicId + ".json", IndividualTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicResponse getMultipleTopicsById(List<Long> topicIds) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		for (Long id : topicIds) {
			sb.append(id).append(",");
		}
		try {
			return genericGet("topics/show_many?ids=" + sb.toString() + ".json", GroupTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicResponse getTopicsByForum(long forumId) throws ZendeskApiException {
		try {
			return genericGet("forums/" + forumId + "/topics.json", GroupTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicResponse getTopicsByUser(long userId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/topics.json", GroupTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicResponse createTopic(Topic topic) throws ZendeskApiException {
		IndividualTopicResponse individualTopicResponse = new IndividualTopicResponse();
		individualTopicResponse.setTopic(topic);
		try {
			return genericPost("topics.json", individualTopicResponse, IndividualTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of topic " + topic.getTitle() + " failed", e);
		}
	}

	public IndividualTopicResponse updateTopic(Topic topic) throws ZendeskApiException {
		IndividualTopicResponse individualTopicResponse = new IndividualTopicResponse();
		individualTopicResponse.setTopic(topic);
		try {
			return genericPut("topics/" + topic.getId() + ".json", individualTopicResponse, IndividualTopicResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Updating topic " + topic.getTitle() + " failed", e);
		}
	}

	public boolean deleteTopic(long topicId) throws ZendeskApiException {
		try {
			return genericDelete("topics/" + topicId + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicCommentResponse getTopicCommentsByTopicId(long topicId) throws ZendeskApiException {
		try {
			return genericGet("topics/" + topicId + "/comments.json", GroupTopicCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicCommentResponse getTopicCommentsByUserId(long userId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/topic_comments.json", GroupTopicCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicCommentResponse getSpecificTopicCommentByTopic(long topicId, long commentId) throws ZendeskApiException {
		try {
			return genericGet("topics/" + topicId + "/comments/" + commentId + ".json", IndividualTopicCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicCommentResponse getSpecificTopicCommentByUser(long userId, long commentId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/topic_comments/" + commentId + ".json", IndividualTopicCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicCommentResponse createTopicComment(long topicId, TopicComment topicComment) throws ZendeskApiException {
		IndividualTopicCommentResponse individualTopicCommentResponse = new IndividualTopicCommentResponse();
		individualTopicCommentResponse.setTopicComment(topicComment);
		try {
			return genericPost("topics/" + topicId + "/comments.json", individualTopicCommentResponse, IndividualTopicCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of topic comment " + topicComment.getTopicId() + " failed", e);
		}
	}

	public IndividualTopicCommentResponse updateTopicComment(TopicComment topicComment) throws ZendeskApiException {
		IndividualTopicCommentResponse individualTopicCommentResponse = new IndividualTopicCommentResponse();
		individualTopicCommentResponse.setTopicComment(topicComment);
		try {
			return genericPut("topics/" + topicComment.getTopicId() + "/comments/" + topicComment.getId() + ".json", individualTopicCommentResponse, IndividualTopicCommentResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Update of topic comment " + topicComment.getTopicId() + " failed", e);
		}
	}

	public boolean deleteTopicComment(long topicId, long commentId) throws ZendeskApiException {
		try {
			return genericDelete("topics/" + topicId + "/comments/" + commentId + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicSubscriptionResponse getTopicSubscriptionsByTopic(long topicId) throws ZendeskApiException {
		try {
			return genericGet("topics/" + topicId + "/subscriptions.json", GroupTopicSubscriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicSubscriptionResponse getAllTopicSubscriptions() throws ZendeskApiException {
		try {
			return genericGet("topic_subscriptions.json", GroupTopicSubscriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicSubscriptionResponse getTopicSubscriptionById(long topicSubscriptionId) throws ZendeskApiException {
		try {
			return genericGet("topic_subscriptions/" + topicSubscriptionId + ".json", IndividualTopicSubscriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicSubscriptionResponse createTopicSubscription(long userId, long topicId) throws ZendeskApiException {
		TopicSubscription topicSubscription = new TopicSubscription();
		topicSubscription.setUserId(userId);
		topicSubscription.setTopicId(topicId);
		IndividualTopicSubscriptionResponse individualTopicSubscriptionResponse = new IndividualTopicSubscriptionResponse();
		individualTopicSubscriptionResponse.setTopicSubscription(topicSubscription);
		try {
			return genericPost("topic_subscription.json", individualTopicSubscriptionResponse, IndividualTopicSubscriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of topic subscription with user id " +
					userId + " and topic id " + topicId + " failed", e);
		}
	}

	public boolean deleteTopicSubscription(long topicSubscriptionId) throws ZendeskApiException {
		try {
			return genericDelete("topic_subscriptions/" + topicSubscriptionId + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicVoteResponse getTopicVotes(long topicId) throws ZendeskApiException {
		try {
			return genericGet("topics/" + topicId + "/votes.json", GroupTopicVoteResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupTopicVoteResponse getTopicVotesByUser(long userId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/topic_votes.json", GroupTopicVoteResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * Checks to see if the current user has cast a vote in this topic. Returns
	 * null if not
	 * 
	 * @param topicId
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualTopicVoteResponse checkForVote(long topicId) throws ZendeskApiException {
		try {
			return genericGet("topics/" + topicId + "/vote.json", IndividualTopicVoteResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualTopicVoteResponse createVote(long topicId) throws ZendeskApiException {
		try {
			return genericPost("topics/" + topicId + "/vote.json", new TopicVote(), IndividualTopicVoteResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public boolean deleteVote(long topicId) throws ZendeskApiException {
		try {
			return genericDelete("topics/" + topicId + "/vote.json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

}