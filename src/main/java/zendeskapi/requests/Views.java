package zendeskapi.requests;

import java.util.List;

import zendeskapi.StringUtils;
import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.views.GroupViewCountResponse;
import zendeskapi.models.views.GroupViewResponse;
import zendeskapi.models.views.IndividualViewCountResponse;
import zendeskapi.models.views.IndividualViewResponse;
import zendeskapi.models.views.executed.ExecutedViewResponse;
import zendeskapi.models.views.executed.PreviewViewRequest;

public class Views extends ZendeskHttpHelper {

	public Views(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	public GroupViewResponse getAllViews() throws ZendeskApiException {
		try {
			return genericGet("views.json", GroupViewResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupViewResponse getActiveViews() throws ZendeskApiException {
		try {
			return genericGet("views/active.json", GroupViewResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupViewResponse getCompactViews() throws ZendeskApiException {
		try {
			return genericGet("views/compact.json", GroupViewResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualViewResponse getView(long id) throws ZendeskApiException {
		try {
			return genericGet("views/" + id + ".json", IndividualViewResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public ExecutedViewResponse executeView(long id, String sortCol, boolean ascending) throws ZendeskApiException {
		String resource = "views/" + id + "/execute.json";
		if (!StringUtils.isNullOrEmpty(sortCol)) {
			String asc = ascending ? "" : "desc";
			resource += "?sort_by=" + sortCol + "&sort_order=" + asc;
		}
		try {
			return genericGet(resource, ExecutedViewResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public ExecutedViewResponse previewView(PreviewViewRequest preview) throws ZendeskApiException {
		try {
			return genericPost("views/preview.json", preview, ExecutedViewResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupViewCountResponse getViewCounts(List<Long> viewIds) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		for (Long id : viewIds) {
			sb.append(id).append(",");
		}
		try {
			return genericGet("views/count_many.json?ids=" + sb.toString(), GroupViewCountResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualViewCountResponse getViewCount(long viewId) throws ZendeskApiException {
		try {
			return genericGet("views/" + viewId + "/count.json", IndividualViewCountResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}
}
