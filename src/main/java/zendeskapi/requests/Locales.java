package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.locales.GroupLocaleResponse;
import zendeskapi.models.locales.IndividualLocaleResponse;

public class Locales extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Locales(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * This lists the translation locales that are available for the account.
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupLocaleResponse getAllLocales() throws ZendeskApiException {
		try {
			return genericGet("locales.json", GroupLocaleResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to get a list of all locales", e);
		}
	}

	/**
	 * This lists the translation locales that have been localised for agents.
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupLocaleResponse getLocalesForAgents() throws ZendeskApiException {
		try {
			return genericGet("locales/agent.json", GroupLocaleResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to get a list locales for agents that have been localised", e);
		}
	}

	/**
	 * This lists the translation locales that have been localised for agents with <code>id</code>.
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualLocaleResponse getLocaleById(long id) throws ZendeskApiException {
		try {
			return genericGet("locales/" + id + ".json", IndividualLocaleResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to get a list locales with id " + id + " for agents that have been localised", e);
		}
	}

	/**
	 * This works exactly like show, but instead of taking an id as argument,
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualLocaleResponse getCurrentLocale() throws ZendeskApiException {
		try {
			return genericGet("locales/current.json", IndividualLocaleResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to get current locale", e);
		}
	}
}
