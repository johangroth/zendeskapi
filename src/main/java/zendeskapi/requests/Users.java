package zendeskapi.requests;

import java.util.List;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.extensions.JsonHelper;
import zendeskapi.models.shared.JobStatusResponse;
import zendeskapi.models.users.GroupUserIdentityResponse;
import zendeskapi.models.users.GroupUserResponse;
import zendeskapi.models.users.IndividualUserIdentityResponse;
import zendeskapi.models.users.IndividualUserResponse;
import zendeskapi.models.users.Password;
import zendeskapi.models.users.User;
import zendeskapi.models.users.UserIdentity;

public class Users extends ZendeskHttpHelper {
	public Users(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	public IndividualUserResponse getCurrentUser() throws ZendeskApiException {
		try {
			return genericGet("users/me.json", IndividualUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupUserResponse getAllUsers() throws ZendeskApiException {
		try {
			return genericGet("users.json", GroupUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualUserResponse getUser(long id) throws ZendeskApiException {
		try {
			return genericGet("users/" + id + ".json", IndividualUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupUserResponse searchByEmail(String email) throws ZendeskApiException {
		try {
			return genericGet("users/search.json?query=" + email, GroupUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupUserResponse searchByExternalId(String externalId) throws ZendeskApiException {
		try {
			return genericGet("users/search.json?external_id=" + externalId, GroupUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupUserResponse getUsersInGroup(long id) throws ZendeskApiException {
		try {
			return genericGet("groups/" + id + "/users.json", GroupUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public GroupUserResponse getUsersInOrganization(long id) throws ZendeskApiException {
		try {
			return genericGet("organizations/" + id + "/users.json", GroupUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public IndividualUserResponse createUser(User user) throws ZendeskApiException {
		IndividualUserResponse body = new IndividualUserResponse();
		body.setUser(user);
		try {
			return genericPost("users.json", body, IndividualUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to create user " + user.getName(), e);
		}
	}

	public JobStatusResponse bulkCreateUsers(List<User> users) throws ZendeskApiException {
		GroupUserResponse userList = new GroupUserResponse();
		userList.setUsers(users);
		try {
			return genericPost("users/create_many.json", userList, JobStatusResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Faild to bulk create users", e);
		}
	}

	public IndividualUserResponse suspendUser(long id) throws ZendeskApiException {
		User user = new User();
		user.setId(id);
		user.setSuspended(true);
		return updateUser(user);
	}

	public IndividualUserResponse updateUser(User user) throws ZendeskApiException {
		IndividualUserResponse userToUpdate = new IndividualUserResponse();
		userToUpdate.setUser(user);
		try {
			return genericPut(getUserIdUri(user.getId()) + ".json", userToUpdate, IndividualUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Could not update user id " + user.getId(), e);
		}
	}

	public boolean deleteUser(long id) throws ZendeskApiException {
		try {
			return genericDelete("users/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public boolean setUsersPassword(long userId, String newPassword) throws ZendeskApiException {
		String request = "users/" + userId + "/password.json";
		try {
			return genericBooleanPost(request, newPassword);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	public boolean changeUsersPassword(long userId, String oldPassword, String newPassword) throws ZendeskApiException {
		Password body = new Password();
		body.setPreviousPassword(oldPassword);
		body.setPassword(newPassword);
		try {
			String json = JsonHelper.marshal(body);
			return genericBooleanPost(getUserIdUri(userId) + "/password.json", json);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to change the password for user id " + userId, e);
		}
	}

	public GroupUserIdentityResponse getUserIdentities(long userId) throws ZendeskApiException {
		try {
			return genericGet(getUserIdUri(userId) + "/identities.json", GroupUserIdentityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting identities for user id " + userId + "failed", e);
		}
	}

	public IndividualUserIdentityResponse getSpecificUserIdentity(long userId, long identityId) throws ZendeskApiException {
		try {
			return genericGet(getUserIdentityIdUri(userId, identityId) + ".json", IndividualUserIdentityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting identity id " + identityId + " for user id " + userId + "failed", e);
		}
	}

	public IndividualUserIdentityResponse addUserIdentity(long userId, UserIdentity identity) throws ZendeskApiException {
		try {
			return genericPost(getUserIdUri(userId) + "/identities.json", identity, IndividualUserIdentityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Could not add user identity to user id: " + userId, e);
		}
	}

	public IndividualUserIdentityResponse setUserIdentityAsVerified(long userId, long identityId) throws ZendeskApiException {
		User nullUser = new User();
		try {
			return genericPut(getUserIdentityIdUri(userId, identityId) + "/verify.json", nullUser, IndividualUserIdentityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Could not verify identity id " + identityId + " with user id " + userId, e);
		}
	}

	public GroupUserIdentityResponse setUserIdentityAsPrimary(long userId, long identityId) throws ZendeskApiException {
		User nullUser = new User();
		try {
			return genericPut(getUserIdentityIdUri(userId, identityId) + "/make_primary.json", nullUser, GroupUserIdentityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Could not set identity id " + identityId + " for user id " + userId + " as primary", e);
		}
	}

	/**
	 * This sends a verification email to the user, asking him to click a link
	 * in order to verify ownership of the email address
	 * 
	 * @param userId
	 * @param identityId
	 * @return
	 */
	public IndividualUserIdentityResponse sendUserVerificationRequest(long userId, long identityId) throws ZendeskApiException {
		User nullUser = new User();
		try {
			return genericPut(getUserIdentityIdUri(userId, identityId) + "/request_verification.json", nullUser, IndividualUserIdentityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Sending of verification email for identity id " + identityId + " of user id " + userId + " failed", e);
		}
	}

	public boolean deleteUserIdentity(long userId, long identityId) throws ZendeskApiException {
		try {
			return genericDelete(getUserIdentityIdUri(userId, identityId) + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	private String getUserIdUri(long userId) {
		return "users/" + userId;
	}

	private String getIdentityIdUri(long identityId) {
		return "/identities/" + identityId;
	}

	private String getUserIdentityIdUri(long userId, long identityId) {
		return getUserIdUri(userId) + getIdentityIdUri(identityId);
	}
}
