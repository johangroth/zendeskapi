package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.satisfactionratings.GroupSatisfactionResponse;
import zendeskapi.models.satisfactionratings.IndividualSatisfactionResponse;
import zendeskapi.models.satisfactionratings.SatisfactionRating;

public class SatisfactionRatings extends ZendeskHttpHelper {

	public SatisfactionRatings(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * Lists all received satisfaction rating requests ever issued for your
	 * account. To only list the satisfaction ratings submitted by your
	 * customers, use the "received" end point below instead.
	 * 
	 * @return GroupSatisfactionResponse
	 * @throws ZendeskApiException
	 */
	public GroupSatisfactionResponse getSatisfactionRatings() throws ZendeskApiException {
		try {
			return genericGet("satisfaction_ratings.json", GroupSatisfactionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Listing of all satisfaction ratings failed", e);
		}
	}

	/**
	 * Lists satisfaction ratings provided by customers.
	 * 
	 * @return GroupSatisfactionResponse
	 * @throws ZendeskApiException
	 */
	public GroupSatisfactionResponse getReceivedSatisfactionRatings() throws ZendeskApiException {
		try {
			return genericGet("satisfaction_ratings/received.json", GroupSatisfactionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Listing of satisfaction ratings provided by customers failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return IndividualSatisfactionResponse
	 * @throws ZendeskApiException
	 */
	public IndividualSatisfactionResponse getSatisfactionRatingById(long id) throws ZendeskApiException {
		try {
			return genericGet("satisfaction_ratings/" + id + ".json", IndividualSatisfactionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting satisfaction rating for id " + id + " failed", e);
		}
	}

	/**
	 * 
	 * @param ticketId
	 * @param satisfactionRating
	 * @return IndividualSatisfactionResponse
	 * @throws ZendeskApiException
	 */
	public IndividualSatisfactionResponse createSatisfactionRating(long ticketId, SatisfactionRating satisfactionRating) throws ZendeskApiException {
		IndividualSatisfactionResponse individualSatisfactionResponse = new IndividualSatisfactionResponse();
		individualSatisfactionResponse.setSatisfactionRating(satisfactionRating);
		try {
			return genericPost("tickets/" + ticketId + "/satisfaction_rating.json", satisfactionRating, IndividualSatisfactionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creating satisfaction rating " + satisfactionRating.getComment() + " faile", e);
		}
	}
}
