package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.forums.Forum;
import zendeskapi.models.forums.ForumSubscription;
import zendeskapi.models.forums.GroupForumResponse;
import zendeskapi.models.forums.GroupForumSubcriptionResponse;
import zendeskapi.models.forums.IndividualForumResponse;
import zendeskapi.models.forums.IndividualForumSubcriptionResponse;

public class Forums extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Forums(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return GroupForumResponse
	 * @throws ZendeskApiException
	 */
	public GroupForumResponse getForums() throws ZendeskApiException {
		try {
			return genericGet("forums.json", GroupForumResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param forumId
	 * @return IndividualForumResponse
	 * @throws ZendeskApiException
	 */
	public IndividualForumResponse getForumById(long forumId) throws ZendeskApiException {
		try {
			return genericGet("forums/" + forumId + ".json", IndividualForumResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param categoryId
	 * @return IndividualForumResponse
	 * @throws ZendeskApiException
	 */
	public GroupForumResponse getForumsByCategory(long categoryId) throws ZendeskApiException {
		try {
			return genericGet("categories/" + categoryId + "/forums.json", GroupForumResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param forum
	 * @return IndividualForumResponse
	 * @throws ZendeskApiException
	 */
	public IndividualForumResponse createForum(Forum forum) throws ZendeskApiException {
		IndividualForumResponse forumToCreate = new IndividualForumResponse();
		forumToCreate.setForum(forum);
		try {
			return genericPost("forums.json", forumToCreate, IndividualForumResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of forum " + forum.getName() + " failed", e);
		}
	}

	/**
	 * 
	 * @param forum
	 * @return IndividualForumResponse
	 * @throws ZendeskApiException
	 */
	public IndividualForumResponse updateForum(Forum forum) throws ZendeskApiException {
		IndividualForumResponse forumToUpdate = new IndividualForumResponse();
		forumToUpdate.setForum(forum);
		try {
			return genericPut("forums/" + forum.getId(), forumToUpdate, IndividualForumResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Updating forum " + forum.getName() + " failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return boolean
	 * @throws ZendeskApiException
	 */
	public boolean deleteForum(long id) throws ZendeskApiException {
		try {
			return genericDelete("forums/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return GroupForumSubcriptionResponse
	 * @throws ZendeskApiException
	 */
	public GroupForumSubcriptionResponse getForumSubscriptions() throws ZendeskApiException {
		try {
			return genericGet("forum_subscriptions.json", GroupForumSubcriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param forumId
	 * @return GroupForumSubcriptionResponse
	 * @throws ZendeskApiException
	 */
	public GroupForumSubcriptionResponse getForumSubscriptionsByForumId(long forumId) throws ZendeskApiException {
		try {
			return genericGet("forums/" + forumId + "/subscriptions.json", GroupForumSubcriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param subscriptionId
	 * @return IndividualForumSubcriptionResponse
	 * @throws ZendeskApiException
	 */
	public IndividualForumSubcriptionResponse getForumSubscriptionsById(long subscriptionId) throws ZendeskApiException {
		try {
			return genericGet("forum_subscriptions/" + subscriptionId + ".json", IndividualForumSubcriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param forumSubscription
	 * @return IndividualForumSubcriptionResponse
	 * @throws ZendeskApiException
	 */
	public IndividualForumSubcriptionResponse createForumSubscription(ForumSubscription forumSubscription) throws ZendeskApiException {
		IndividualForumSubcriptionResponse forumSubcriptionToCreate = new IndividualForumSubcriptionResponse();
		forumSubcriptionToCreate.setForumSubscription(forumSubscription);
		try {
			return genericPost("forum_subscriptions.json", forumSubcriptionToCreate, IndividualForumSubcriptionResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of forum subscription failed", e);
		}
	}

	/**
	 * 
	 * @param subscriptionId
	 * @return boolean
	 * @throws ZendeskApiException
	 */
	public boolean deleteForumSubscription(long subscriptionId) throws ZendeskApiException {
		try {
			return genericDelete("forum_subscriptions/" + subscriptionId + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException("Deletion of forum subscription with id " + subscriptionId + " failed", e);
		}
	}
}