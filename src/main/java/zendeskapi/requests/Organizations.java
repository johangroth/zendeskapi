package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.organizations.GroupOrganizationResponse;
import zendeskapi.models.organizations.IndividualOrganizationResponse;
import zendeskapi.models.organizations.Organization;

public class Organizations extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Organizations(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * Returns a list of all organisations encapsulated in class
	 * GroupOrganizationResponse.
	 * 
	 * @return GroupOrganizationResponse
	 * @throws ZendeskApiException
	 */
	public GroupOrganizationResponse getOrganizations() throws ZendeskApiException {
		try {
			return genericGet("organizations.json", GroupOrganizationResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting all organizations failed", e);
		}
	}

	/**
	 * Returns an array of organisations whose name starts with the value
	 * specified in the name parameter. The name must be at least 2 characters
	 * in length.
	 * 
	 * @param name
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupOrganizationResponse getOrganizationsStartingWith(String name) throws ZendeskApiException {
		try {
			return genericPost("organizations/autocomplete.json?name=" + name, null, GroupOrganizationResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting all organizations starting with " + name + " failed", e);
		}
	}

	/**
	 * 
	 * @param searchTerm
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupOrganizationResponse searchForOrganizations(String searchTerm) throws ZendeskApiException {
		try {
			return genericGet("organizations/search.json?external_id=" + searchTerm, GroupOrganizationResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting all organizations with search term " + searchTerm + " failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualOrganizationResponse getOrganization(long id) throws ZendeskApiException {
		try {
			return genericGet("organizations/" + id + ".json", IndividualOrganizationResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting organization with id " + id + " failed", e);
		}
	}

	/**
	 * 
	 * @param organization
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualOrganizationResponse createOrganization(Organization organization) throws ZendeskApiException {
		IndividualOrganizationResponse orgToCreate = new IndividualOrganizationResponse();
		orgToCreate.setOrganization(organization);
		try {
			return genericPost("organizations.json", orgToCreate, IndividualOrganizationResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of organization " + organization.getName() + " failed", e);
		}
	}

	/**
	 * 
	 * @param organization
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualOrganizationResponse updateOrganization(Organization organization) throws ZendeskApiException {
		IndividualOrganizationResponse orgToUpdate = new IndividualOrganizationResponse();
		orgToUpdate.setOrganization(organization);
		try {
			return genericPut("organizations/" + organization.getId() + ".json", orgToUpdate, IndividualOrganizationResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Update of organization " + organization.getName() + " failed", e);
		}

	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteOrganization(long id) throws ZendeskApiException {
		try {
			return genericDelete("organizations/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException("Deletion of organization with id " + id + " failed", e);
		}
	}
}