package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.sharingagreements.GroupSharingAgreementResponse;

public class SharingAgreements extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public SharingAgreements(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupSharingAgreementResponse getSharingAgreements() throws ZendeskApiException {
		try {
			return genericGet("sharing_agreements.json", GroupSharingAgreementResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting sharing agreements failed", e);
		}
	}
}
