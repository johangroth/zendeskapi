package zendeskapi.requests;

import java.util.List;

import org.apache.commons.httpclient.util.HttpURLConnection;
import org.joda.time.DateTime;

import zendeskapi.RequestResult;
import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.shared.GroupAuditResponse;
import zendeskapi.models.shared.IndividualAuditResponse;
import zendeskapi.models.shared.JobStatusResponse;
import zendeskapi.models.tickets.BulkUpdate;
import zendeskapi.models.tickets.Comment;
import zendeskapi.models.tickets.GroupTicketFieldResponse;
import zendeskapi.models.tickets.GroupTicketResponse;
import zendeskapi.models.tickets.IndividualTicketFieldResponse;
import zendeskapi.models.tickets.IndividualTicketResponse;
import zendeskapi.models.tickets.Ticket;
import zendeskapi.models.tickets.TicketExportResponse;
import zendeskapi.models.tickets.TicketField;
import zendeskapi.models.tickets.suspended.GroupSuspendedTicketResponse;
import zendeskapi.models.tickets.suspended.IndividualSuspendedTicketResponse;
import zendeskapi.models.users.GroupUserResponse;

public class Tickets extends ZendeskHttpHelper {
	private static final String TICKETS = "tickets";
	private static final String VIEWS = "views";
	private static final String ORGANISATIONS = "organizations";

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Tickets(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getAllTickets() throws ZendeskApiException {
		try {
			return genericGet(TICKETS + ".json", GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getTicketsByViewID(int id) throws ZendeskApiException {
		try {
			String request = VIEWS + id + TICKETS + ".json";
			return genericGet(request, GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getTicketsByOrganizationID(long id) throws ZendeskApiException {
		try {
			String request = ORGANISATIONS + "/" + id + "/" + TICKETS + ".json";
			return genericGet(request, GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getRecentTickets() throws ZendeskApiException {
		try {
			return genericGet("tickets/recent.json", GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getTicketsByUserID(long userId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/tickets/requested.json", GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getTicketsWhereUserIsCopied(long userId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/tickets/ccd.json", GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualTicketResponse getTicket(long id) throws ZendeskApiException {
		try {
			String request = TICKETS + "/" + id + ".json";
			return genericGet(request, IndividualTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ids
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getMultipleTickets(List<Long> ids) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		for (Long id : ids) {
			sb.append(id).append(",");
		}
		String csvIds = sb.toString();
		try {
			return genericGet(TICKETS + "/show_many.json?ids=" + csvIds, GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ticket
	 * @return IndividualTicketResponse
	 * @throws ZendeskApiException
	 */
	public IndividualTicketResponse createTicket(Ticket ticket) throws ZendeskApiException {
		IndividualTicketResponse individualTicketResponse = new IndividualTicketResponse();
		individualTicketResponse.setTicket(ticket);
		try {
			return genericPost("tickets.json", individualTicketResponse, IndividualTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * UpdateTicket a ticket or add comments to it. Keep in mind that some
	 * things like the description can't be updated.
	 * 
	 * @param ticket
	 * @param comment
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualTicketResponse updateTicket(Ticket ticket, Comment comment) throws ZendeskApiException {
		if (comment != null) {
			ticket.setComment(comment);
		}
		IndividualTicketResponse individualTicketResponse = new IndividualTicketResponse();
		individualTicketResponse.setTicket(ticket);
		try {
			return genericPut("tickets/" + ticket.getId() + ".json", individualTicketResponse, IndividualTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ids
	 * @param info
	 * @return
	 * @throws ZendeskApiException
	 */
	public JobStatusResponse bulkUpdate(List<Long> ids, BulkUpdate info) throws ZendeskApiException {
		StringBuilder csvs = new StringBuilder();
		for (Long id : ids) {
			csvs.append(id.toString()).append(",");
		}
		Ticket ticket = convertBulkUpdateToTicket(info);
		try {
			return genericPut("tickets/update_many.json?ids=" + csvs.toString(), ticket, JobStatusResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean delete(long id) throws ZendeskApiException {
		try {
			return genericDelete(TICKETS + "/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ids
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteMultiple(List<Long> ids) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		for (Long id : ids) {
			sb.append(id).append(",");
		}
		try {
			return genericDelete(TICKETS + "/destroy_many.json?ids=" + sb.toString());
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupUserResponse getCollaborators(long id) throws ZendeskApiException {
		try {
			return genericGet(TICKETS + "/" + id + "/collaborators.json", GroupUserResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getIncidents(long id) throws ZendeskApiException {
		try {
			return genericGet(TICKETS + "/" + id + "/incidents.json", GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse getProblems() throws ZendeskApiException {
		try {
			return genericGet("problems.json", GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param text
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketResponse autoCompleteProblems(String text) throws ZendeskApiException {
		try {
			return genericPost("problem/autocomplete.json?text=" + text, null, GroupTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ticketId
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupAuditResponse getAudits(long ticketId) throws ZendeskApiException {
		try {
			return genericGet("tickets/" + ticketId + "/audits.json", GroupAuditResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ticketId
	 * @param auditId
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualAuditResponse getAuditById(long ticketId, long auditId) throws ZendeskApiException {
		String request = "tickets/" + ticketId + "/audits/" + auditId + ".json";
		try {
			return genericGet(request, IndividualAuditResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ticketId
	 * @param auditId
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean markAuditAsTrusted(long ticketId, long auditId) throws ZendeskApiException {
		String resource = "tickets/" + ticketId + "/audits/" + auditId + "/trust.json";
		try {
			RequestResult res = runRequest(resource, RequestMethod.PUT, RequestResult.class);
			return res.isOk();
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param startTime
	 * @return
	 * @throws ZendeskApiException
	 */
	public TicketExportResponse getInrementalTicketExport(DateTime startTime) throws ZendeskApiException {
		try {
			return genericGet("exports/tickets.json?start_time=" + startTime.getMillis() / 1000, TicketExportResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * Since the other method can only be called once every 5 minutes it is not
	 * suitable for Automated tests.
	 * 
	 * @param startTime
	 * @return
	 * @throws ZendeskApiException
	 */
	public TicketExportResponse testOnly_GetInrementalTicketExport(DateTime startTime) throws ZendeskApiException {
		try {
			return genericGet("exports/tickets/sample.json?start_time=" + startTime.getMillis() / 1000, TicketExportResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTicketFieldResponse getTicketFields() throws ZendeskApiException {
		try {
			return genericGet("ticket_fields.json", GroupTicketFieldResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ticketField
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualTicketFieldResponse createTicketField(TicketField ticketField) throws ZendeskApiException {
		IndividualTicketFieldResponse individualTicketFieldResponse = new IndividualTicketFieldResponse();
		individualTicketFieldResponse.setTicketField(ticketField);
		try {
			return genericPost("ticket_fields.json", individualTicketFieldResponse, IndividualTicketFieldResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ticketField
	 * @return
	 */
	public IndividualTicketFieldResponse updateTicketField(TicketField ticketField) throws ZendeskApiException {
		IndividualTicketFieldResponse individualTicketFieldResponse = new IndividualTicketFieldResponse();
		individualTicketFieldResponse.setTicketField(ticketField);
		try {
			return genericPut("ticket_fields/" + ticketField.getId() + ".json", individualTicketFieldResponse, IndividualTicketFieldResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteTicketField(long id) throws ZendeskApiException {
		try {
			return genericDelete("ticket_fields/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupSuspendedTicketResponse getSuspendedTickets() throws ZendeskApiException {
		try {
			return genericGet("suspended_tickets.json", GroupSuspendedTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualSuspendedTicketResponse getSuspendedTicketById(long id) throws ZendeskApiException {
		try {
			return genericGet("suspended_tickets/" + id + ".json", IndividualSuspendedTicketResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean recoverSuspendedTicket(long id) throws ZendeskApiException {
		String resource = "suspended_tickets/" + id + "/recover.json";
		try {
			RequestResult res = runRequest(resource, RequestMethod.PUT, RequestResult.class);
			return res.getHttpStatusCode() == HttpURLConnection.HTTP_OK;
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ids
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean recoverManySuspendedTickets(List<Long> ids) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		for (Long id : ids) {
			sb.append(id).append(",");
		}
		String resource = "suspended_tickets/recover_many.json?ids=" + sb.toString();
		try {
			RequestResult res = runRequest(resource, RequestMethod.PUT, RequestResult.class);
			return res.getHttpStatusCode() == HttpURLConnection.HTTP_OK;
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteSuspendedTickets(long id) throws ZendeskApiException {
		try {
			return genericDelete("suspended_tickets/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param ids
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteManySuspendedTickets(List<Long> ids) throws ZendeskApiException {
		StringBuilder sb = new StringBuilder();
		for (Long id : ids) {
			sb.append(id).append(",");
		}
		try {
			return genericDelete("suspended_tickets/destroy_many.json?ids=" + sb.toString());
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	private Ticket convertBulkUpdateToTicket(BulkUpdate bulk) {
		Ticket ticket = new Ticket();
		ticket.setType(bulk.getType());
		ticket.setStatus(bulk.getStatus().getValue());
		ticket.setTags(bulk.getTags());
		ticket.setSubject(bulk.getSubject());
		ticket.setComment(bulk.getComment());
		ticket.setAssigneeId(bulk.getAssigneeId());
		ticket.setCollaboratorEmails(bulk.getCollaboratorEmails());
		return ticket;
	}
}
