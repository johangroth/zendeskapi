package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.tags.GroupTagResult;
import zendeskapi.models.tags.TagAutocompleteResponse;

public class Tags extends ZendeskHttpHelper {
	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Tags(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupTagResult getTags() throws ZendeskApiException {
		try {
			return genericGet("tags.json", GroupTagResult.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting all tags failed", e);
		}
	}

	/**
	 * Returns an array of registered and recent tag names that start with the
	 * specified name. The name must be at least 2 characters in length.
	 * @param name
	 * @return
	 */
	public TagAutocompleteResponse autocompleteTags(String name) throws ZendeskApiException {
		try {
			return genericPost("autocomplete/tags.json?name=" + name, null, TagAutocompleteResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting tags beginning with " + name + " failed", e);
		}
	}

}
