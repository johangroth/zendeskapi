package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.accountsandactivities.GroupActivityResponse;
import zendeskapi.models.accountsandactivities.IndividualActivityResponse;
import zendeskapi.models.accountsandactivities.SettingsResponse;

public class AccountsAndActivity extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public AccountsAndActivity(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public SettingsResponse getSettings() throws ZendeskApiException {
		try {
			return genericGet("account/settings.json", SettingsResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting all settings failed", e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupActivityResponse getActivities() throws ZendeskApiException {
		try {
			return genericGet("activities.json", GroupActivityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting all activities failed", e);
		}
	}

	/**
	 * 
	 * @param activityId
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualActivityResponse getActivityById(long activityId) throws ZendeskApiException {
		try {
			return genericGet("activities/" + activityId + ".json", IndividualActivityResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting activity with id " + activityId + " failed", e);
		}
	}
}
