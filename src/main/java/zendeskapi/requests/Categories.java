package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.categories.Category;
import zendeskapi.models.categories.GroupCategoryResponse;
import zendeskapi.models.categories.IndividualCategoryResponse;

public class Categories extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Categories(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupCategoryResponse getCategories() throws ZendeskApiException {
		try {
			return genericGet("categories.json", GroupCategoryResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting categories failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualCategoryResponse getCategoryById(long id) throws ZendeskApiException {
		try {
			return genericGet("categories/" + id + ".json", IndividualCategoryResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting category with id " + id + " failed", e);
		}
	}

	/**
	 * 
	 * @param category
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualCategoryResponse createCategory(Category category) throws ZendeskApiException {
		IndividualCategoryResponse categoryToCreate = new IndividualCategoryResponse();
		categoryToCreate.setCategory(category);
		try {
			return genericPost("categories.json", categoryToCreate, IndividualCategoryResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Faled to create categery " + category.getName(), e);
		}
	}

	/**
	 * 
	 * @param category
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualCategoryResponse updateCategory(Category category) throws ZendeskApiException {
		IndividualCategoryResponse categoryToUpdate = new IndividualCategoryResponse();
		categoryToUpdate.setCategory(category);
		try {
			return genericPut("categories/" + category.getId() + ".json", categoryToUpdate, IndividualCategoryResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Update of category " + category.getName() + " failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteCategory(long id) throws ZendeskApiException {
		try {
			return genericDelete("categories/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException("Deletion of category with id " + id + " failed", e);
		}
	}
}