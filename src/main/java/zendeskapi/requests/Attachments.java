package zendeskapi.requests;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.httpclient.util.HttpURLConnection;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.extensions.JsonHelper;
import zendeskapi.models.shared.Upload;
import zendeskapi.models.shared.UploadResult;
import zendeskapi.models.shared.ZendeskFile;

public class Attachments extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Attachments(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @param file
	 * @return
	 * @throws ZendeskApiException
	 */
	public Upload uploadAttachment(ZendeskFile file) throws ZendeskApiException {
		return uploadAttachment(file, "");
	}

	/**
	 * 
	 * @param files
	 * @return
	 * @throws ZendeskApiException
	 */
	public Upload uploadAttachments(List<ZendeskFile> files) throws ZendeskApiException {
		if (files.isEmpty()) {
			return null;
		}

		Upload res = null;
		for (ZendeskFile file : files) {
			if (res != null) {
				res = uploadAttachment(file, res.getToken());
			} else {
				res = uploadAttachment(file);
			}
		}
		
		return res;
	}

	/**
	 * Uploads a file to zendesk and returns the corresponding token id. To
	 * upload another file to an existing token just pass in the existing token
	 * 
	 * @param file
	 * @param token
	 * @return
	 * @throws ZendeskApiException
	 */
	public Upload uploadAttachment(ZendeskFile file, String token) throws ZendeskApiException {
		String requestUrl = zendeskUrl;
		if (!requestUrl.endsWith("/")) {
			requestUrl += "/";
		}

		requestUrl += "uploads.json?filename=" + file.getFileName();
		if (token != null && token.length() > 0) {
			requestUrl += "&token=" + token;
		}

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(requestUrl);
		StringEntity entity = new StringEntity(new String(file.getFileData()), ContentType.create("application/binary", (Charset)null));
		post.setHeader("Accept", ContentType.APPLICATION_JSON.getMimeType());
		post.setHeader("Authorization", getAuthHeader(user, password));
		post.setEntity(entity);
		try {
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() != HttpURLConnection.HTTP_CREATED) {
				throw new ZendeskApiException("Upload of attachment failed\n" + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			UploadResult uploadResult = JsonHelper.unmarshal(sb.toString(), UploadResult.class);
			return uploadResult.getUpload();
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}
}
