package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.models.triggers.GroupTriggerResponse;
import zendeskapi.models.triggers.IndividualTriggerResponse;

public class Triggers extends ZendeskHttpHelper {
	public Triggers(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	public GroupTriggerResponse getTriggers() throws Exception {
		return genericGet("triggers.json", GroupTriggerResponse.class);
	}

	public IndividualTriggerResponse getTriggerById(long id) throws Exception {
		return genericGet("triggers/" + id + ".json", IndividualTriggerResponse.class);
	}

	public GroupTriggerResponse getActiveTriggers() throws Exception {
		return genericGet("triggers/active.json", GroupTriggerResponse.class);
	}

}
