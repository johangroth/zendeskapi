package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.customroles.CustomRoles;

public class CustomAgentRoles extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public CustomAgentRoles(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public CustomRoles getCustomRoles() throws ZendeskApiException {
		try {
			return genericGet("custom_roles.json", CustomRoles.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Getting custom roles failed", e);
		}
	}
}
