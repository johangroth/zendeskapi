package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.groups.Group;
import zendeskapi.models.groups.GroupMembership;
import zendeskapi.models.groups.IndividualGroupMembershipResponse;
import zendeskapi.models.groups.IndividualGroupResponse;
import zendeskapi.models.groups.MultipleGroupMembershipResponse;
import zendeskapi.models.groups.MultipleGroupResponse;

public class Groups extends ZendeskHttpHelper {
	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Groups(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupResponse getGroups() throws ZendeskApiException {
		try {
			return genericGet("groups.json", MultipleGroupResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupResponse getAssignableGroups() throws ZendeskApiException {
		try {
			return genericGet("groups/assignable.json", MultipleGroupResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualGroupResponse getGroupById(long id) throws ZendeskApiException {
		try {
			return genericGet("groups/" + id + ".json", IndividualGroupResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param groupName
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualGroupResponse createGroup(String groupName) throws ZendeskApiException {
		IndividualGroupResponse individualGroupResponse = new IndividualGroupResponse();
		Group group = new Group();
		group.setName(groupName);
		individualGroupResponse.setGroup(group);
		try {
			return genericPost("groups.json", individualGroupResponse, IndividualGroupResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to create group " + groupName, e);
		}
	}

	/**
	 * 
	 * @param group
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualGroupResponse updateGroup(Group group) throws ZendeskApiException {
		IndividualGroupResponse individualGroupResponse = new IndividualGroupResponse();
		individualGroupResponse.setGroup(group);
		try {
			return genericPut("groups/" + group.getId(), individualGroupResponse, IndividualGroupResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Update of group " + group.getName() + " failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteGroup(long id) throws ZendeskApiException {
		try {
			return genericDelete("groups/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupMembershipResponse getGroupMemberships() throws ZendeskApiException {
		try {
			return genericGet("group_memberships.json", MultipleGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupMembershipResponse getGroupMembershipsByUser(long userId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/group_memberships.json", MultipleGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param groupId
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupMembershipResponse getGroupMembershipsByGroup(long groupId) throws ZendeskApiException {
		try {
			return genericGet("groups/" + groupId + "/memberships.json", MultipleGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupMembershipResponse getAssignableGroupMemberships() throws ZendeskApiException {
		try {
			return genericGet("group_memberships/assignable.json", MultipleGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param groupId
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupMembershipResponse getAssignableGroupMembershipsByGroup(long groupId) throws ZendeskApiException {
		try {
			return genericGet("groups/" + groupId + "/memberships/assignable.json", MultipleGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param groupMembershipId
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualGroupMembershipResponse getGroupMembershipsByMembershipId(long groupMembershipId) throws ZendeskApiException {
		try {
			return genericGet("group_memberships/" + groupMembershipId + ".json", IndividualGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param userId
	 * @param groupMembershipId
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualGroupMembershipResponse getGroupMembershipsByUserAndMembershipId(long userId, long groupMembershipId) throws ZendeskApiException {
		try {
			return genericGet("users/" + userId + "/group_memberships/" + groupMembershipId + ".json", IndividualGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * Creating a membership means assigning an agent to a given group
	 * 
	 * @param groupMembership
	 * @return
	 */
	public IndividualGroupMembershipResponse createGroupMembership(GroupMembership groupMembership) throws ZendeskApiException {
		IndividualGroupMembershipResponse individualGroupMembershipResponse = new IndividualGroupMembershipResponse();
		individualGroupMembershipResponse.setGroupMembership(groupMembership);
		try {
			return genericPost("group_memberships.json", individualGroupMembershipResponse, IndividualGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of group membership failed", e);
		}
	}

	/**
	 * 
	 * @param userId
	 * @param groupMembershipId
	 * @return
	 * @throws ZendeskApiException
	 */
	public MultipleGroupMembershipResponse setGroupMembershipAsDefault(long userId, long groupMembershipId) throws ZendeskApiException {
		try {
			return genericPut("users/" + userId + "/group_memberships/" + groupMembershipId + "/make_default.json", new GroupMembership(), MultipleGroupMembershipResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to set default group memebership to " + groupMembershipId + " for user id " + userId, e);
		}
	}

	/**
	 * 
	 * @param groupMembershipId
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteGroupMembership(long groupMembershipId) throws ZendeskApiException {
		try {
			return genericDelete("group_memberships/" + groupMembershipId + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param userId
	 * @param groupMembershipId
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteUserGroupMembership(long userId, long groupMembershipId) throws ZendeskApiException {
		try {
			return genericDelete("users/" + userId + "/group_memberships/" + groupMembershipId + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}
}