package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.StringUtils;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.search.SearchResults;

/**
 * The search API is a unified search API that returns tickets, users, organisations, and forum topics. 
 * Define filters to narrow your search results according to result type, date attributes 
 * and object attributes such as ticket requester or tag.
 * @author jgroth
 *
 */
public class Search extends ZendeskHttpHelper {
	public Search(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @param searchTerm
	 * @param sortBy
	 *            Possible values are 'updated_at', 'created_at', 'priority',
	 *            'status', and 'ticket_type'
	 * @param sortOrder
	 *            Possible values are 'relevance', 'asc', 'desc'. Defaults to
	 *            'relevance' when no 'order' criteria is requested.
	 * @return SearchResults
	 * @throws Exception
	 */
	public SearchResults searchFor(String searchTerm, String sortBy, String sortOrder) throws ZendeskApiException {
		String resource = "search.json?query=" + searchTerm;

		if (!StringUtils.isNullOrEmpty(sortBy)) {
			resource += "&sort_by=" + sortBy;
		}

		if (!StringUtils.isNullOrEmpty(sortOrder)) {
			resource += "&sort_order=" + sortOrder;
		}

		try {
			return genericGet(resource, SearchResults.class);			
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * 
	 * @param searchTerm
	 * @return SearchResults
	 * @throws Exception
	 */
	public SearchResults searchFor(String searchTerm) throws ZendeskApiException {
		return searchFor(searchTerm, "", "");
	}

	/**
	 * This resource behaves the same as SearchFor, but allows anonymous users
	 * to search public forums
	 * 
	 * @param searchTerm
	 * @param sortBy
	 *            Possible values are 'updated_at', 'created_at', 'priority',
	 *            'status', and 'ticket_type'
	 * @param sortOrder
	 *            Possible values are 'relevance', 'asc', 'desc'. Defaults to
	 *            'relevance' when no 'order' criteria is requested.
	 * @return SearchResults
	 * @throws Exception
	 */
	public SearchResults anonymousSearchFor(String searchTerm, String sortBy, String sortOrder) throws ZendeskApiException {
		String resource = "portal/search.json?query=" + searchTerm;

		if (!StringUtils.isNullOrEmpty(sortBy)) {
			resource += "&sort_by=" + sortBy;
		}

		if (!StringUtils.isNullOrEmpty(sortOrder)) {
			resource += "&sort_order=" + sortOrder;
		}

		try {
			return genericGet(resource, SearchResults.class);			
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}
}
