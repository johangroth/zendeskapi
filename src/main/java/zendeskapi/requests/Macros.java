package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.macros.ApplyMacroResponse;
import zendeskapi.models.macros.GroupMacroResponse;
import zendeskapi.models.macros.IndividualMacroResponse;
import zendeskapi.models.macros.Macro;

public class Macros extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public Macros(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * Lists all shared and personal macros available to the current user
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupMacroResponse getAllMacros() throws ZendeskApiException {
		try {
			return genericGet("macros.json", GroupMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to list all macros for current user", e);
		}
	}

	public IndividualMacroResponse getMacroById(long id) throws ZendeskApiException {
		try {
			return genericGet("macros/" + id + ".json", IndividualMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException(e);
		}
	}

	/**
	 * Lists all active shared and personal macros available to the current
	 * 
	 * @return
	 * @throws ZendeskApiException
	 */
	public GroupMacroResponse getActiveMacros() throws ZendeskApiException {
		try {
			return genericGet("macros/active.json", GroupMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Failed to list all active shared and personal macros for current user", e);
		}
	}

	/**
	 * 
	 * @param macro
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualMacroResponse createMacro(Macro macro) throws ZendeskApiException {
		IndividualMacroResponse individualMacroResponse = new IndividualMacroResponse();
		individualMacroResponse.setMacro(macro);
		try {
			return genericPost("macros.json", individualMacroResponse, IndividualMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Creation of macro " + macro.getTitle() + " failed", e);
		}
	}

	/**
	 * 
	 * @param macro
	 * @return
	 * @throws ZendeskApiException
	 */
	public IndividualMacroResponse updateMacro(Macro macro) throws ZendeskApiException {
		IndividualMacroResponse individualMacroResponse = new IndividualMacroResponse();
		individualMacroResponse.setMacro(macro);
		try {
			return genericPut("macros/" + macro.getId() + ".json", individualMacroResponse, IndividualMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Update of macro " + macro.getTitle() + " failed", e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public boolean deleteMacro(long id) throws ZendeskApiException {
		try {
			return genericDelete("macros/" + id + ".json");
		} catch (Exception e) {
			throw new ZendeskApiException("Deletion of macro with id " + id + " failed", e);
		}
	}

	/**
	 * Applies a macro to all applicable tickets.
	 * 
	 * @param macroId
	 * @return
	 * @throws ZendeskApiException
	 */
	public ApplyMacroResponse applyMacro(long macroId) throws ZendeskApiException {
		try {
			return genericGet("macros/" + macroId + "/apply.json", ApplyMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Applying macro with id " + macroId + " failed", e);
		}
	}

	/**
	 * Applies a macro to a specific ticket
	 * 
	 * @param ticketId
	 * @param macroId
	 * @return
	 * @throws ZendeskApiException
	 */
	public ApplyMacroResponse applyMacroToTicket(long ticketId, long macroId) throws ZendeskApiException {
		try {
			return genericGet("tickets/" + ticketId + "/macros/" + macroId + "/apply.json", ApplyMacroResponse.class);
		} catch (Exception e) {
			throw new ZendeskApiException("Applying macro with id " + macroId + " for ticket with id " + ticketId + " failed", e);
		}
	}
}
