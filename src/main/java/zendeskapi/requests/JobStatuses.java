package zendeskapi.requests;

import zendeskapi.ZendeskHttpHelper;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.shared.JobStatusResponse;

public class JobStatuses extends ZendeskHttpHelper {

	/**
	 * 
	 * @param yourZendeskUrl
	 * @param user
	 * @param password
	 */
	public JobStatuses(String yourZendeskUrl, String user, String password) {
		super(yourZendeskUrl, user, password);
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ZendeskApiException
	 */
	public JobStatusResponse getJobStatus(String id) throws ZendeskApiException {
		try {
			return genericGet("job_statuses/" + id + ".json", JobStatusResponse.class);			
		} catch (Exception e) {
			throw new ZendeskApiException("Could not get job status for id " + id, e);
		}
	}

}