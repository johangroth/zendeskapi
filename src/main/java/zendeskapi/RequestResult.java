package zendeskapi;

import java.net.HttpURLConnection;

import zendeskapi.models.ZendeskObject;

public class RequestResult implements ZendeskObject {
	private int httpStatusCode;
	private String content;

	public boolean isOk() {
		return httpStatusCode == HttpURLConnection.HTTP_OK;
	}

	public boolean isCreated() {
		return httpStatusCode == HttpURLConnection.HTTP_CREATED;
	}

	public boolean isNotOk() {
		return httpStatusCode != HttpURLConnection.HTTP_OK;
	}

	public boolean isNotCreated() {
		return httpStatusCode != HttpURLConnection.HTTP_CREATED;
	}

	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
