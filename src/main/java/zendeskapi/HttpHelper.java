package zendeskapi;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import zendeskapi.exception.ZendeskApiException;

public class HttpHelper {
	private static final Logger LOG = Logger.getLogger(HttpHelper.class.getName());

	private static final String JSON ="application/json";
	private static final String BINARY = "application/binary";
	private static final String ACCEPT_HEADER_NAME = "Accept";

	//logging messages
	private static final String EXECUTION_OF_HTTP_METHOD = "Execution of http method ";

	public HttpResponse post(String url, String payload, String auth) {
		HttpPost request = new HttpPost(url);

		StringEntity stringEntity;
		stringEntity = new StringEntity(payload, ContentType.APPLICATION_JSON);
		request.setHeader(ACCEPT_HEADER_NAME, JSON);
		request.setEntity(stringEntity);

		return httpRequestExecute(request, auth);
	}

	public HttpResponse post(String url, byte[] payload, String auth) {
		HttpPost request = new HttpPost(url);
		request.setHeader(ACCEPT_HEADER_NAME, JSON);
		ByteArrayEntity bae = new ByteArrayEntity(payload, ContentType.create(BINARY, Consts.UTF_8));
		request.setEntity(bae);
		return httpRequestExecute(request, auth);
	}

	public HttpResponse put(String url, String payload, String auth) {
		HttpPut request = new HttpPut(url);

		StringEntity stringEntity;
		stringEntity = new StringEntity(payload, ContentType.APPLICATION_JSON);
		request.setHeader(ACCEPT_HEADER_NAME, JSON);
		request.setEntity(stringEntity);

		return httpRequestExecute(request, auth);
	}

	public HttpResponse get(String url, String auth) {
		HttpGet request = new HttpGet(url);
		return httpRequestExecute(request, auth);
	}

	public HttpResponse delete(String url, String auth) {
		HttpDelete request = new HttpDelete(url);
		return httpRequestExecute(request, auth);
	}

	public HttpResponse httpRequestExecute(HttpRequestBase httpRequest, String auth) {
		HttpClient client = getNewHttpClient();

		String authorizationString = new String(Base64.encodeBase64(auth.getBytes(Consts.UTF_8)));
		httpRequest.setHeader("Authorization", "Basic " + authorizationString);

		try {
			return client.execute(httpRequest);
		} catch (Exception e) {
			LOG.error(EXECUTION_OF_HTTP_METHOD + httpRequest.getMethod() + " failed", e);
			throw new ZendeskApiException(e);
		}
	}

	public HttpClient getNewHttpClient() {
		return new DefaultHttpClient();
	}

	public static String readHttpResponseToString(HttpResponse response) {
		try (InputStreamReader inputStreamReader = new InputStreamReader(response.getEntity().getContent(), Consts.UTF_8);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

			StringBuilder stringBuilder = new StringBuilder();
			String line = bufferedReader.readLine();
			while (line != null) {
				stringBuilder.append(line);
				line = bufferedReader.readLine();
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			LOG.error("Reading the http response failed", e);
			throw new ZendeskApiException(e);
		}
	}
}
