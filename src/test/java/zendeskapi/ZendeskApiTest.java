package zendeskapi;

public class ZendeskApiTest {
	protected static final String URL = "https://linuxgrotto1.zendesk.com/api/v2/";
	protected static final String USER = "johangroth1@gmail.com";
	protected static final String PASSWORD = "Winter1234";
	protected static final Long USER_ID = 484350172L;
	protected static final String ORGANISATION = "Linux Grotto";
	protected static final Long ID = 1L;
	protected static final Long ORGANISATION_ID = 29040142L;
	protected static final Long CUSTOM_FIELD_ID = 23869792L;
	protected static final String COLLABORATOR_EMAIL = "johan.groth@linuxgrotto.org.uk";
	protected static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
}
