package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.views.executed.ExecutedViewResponse;

public class ViewsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL,USER, PASSWORD);

	@Test(enabled = false)
	public void testViews() throws Exception {
		long myViewId = 1;
		ExecutedViewResponse view = API.getViews().executeView(myViewId, null, true);
		Assert.assertNotNull(view);
	}
}
