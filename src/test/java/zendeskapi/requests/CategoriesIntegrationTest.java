package zendeskapi.requests;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.categories.Category;
import zendeskapi.models.categories.GroupCategoryResponse;
import zendeskapi.models.categories.IndividualCategoryResponse;

public class CategoriesIntegrationTest extends ZendeskApiTest {

	private List<Long> ids = new ArrayList<Long>();
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);

	@AfterMethod
	public void deleteCreatedCategories() throws Exception {
		for (Long id : ids) {
			API.getCategories().deleteCategory(id);
		}
		ids.clear();
	}

	@Test
	public void testCreateCategory() throws Exception {
		Categories categories = API.getCategories();
		Category category = new Category();
		category.setDescription("Test category description 1");
		category.setName("Test category name 1");
		IndividualCategoryResponse individualCategoryResponse = categories.createCategory(category);
		Category responseCategory = individualCategoryResponse.getCategory();
		Assert.assertEquals(responseCategory.getName(), category.getName());
		Assert.assertEquals(responseCategory.getDescription(), category.getDescription());
		ids.add(responseCategory.getId());
	}

	@Test
	public void testDeleteCategory() throws Exception {
		Categories categories = API.getCategories();
		Category category = new Category();
		category.setDescription("Test category description 1");
		category.setName("Test category name 1");
		IndividualCategoryResponse individualCategoryResponse = categories.createCategory(category);
		Assert.assertTrue(categories.deleteCategory(individualCategoryResponse.getCategory().getId()));
	}

	@Test
	public void testGetCategories() throws Exception {
		createCategories();
		Categories categories = API.getCategories();
		GroupCategoryResponse groupCategoryResponse = categories.getCategories();
		Assert.assertEquals(groupCategoryResponse.getCategories().size(), 3);
		Category category = groupCategoryResponse.getCategories().get(0);
		Assert.assertEquals(category.getName(), "Test category name 1");
		Assert.assertEquals(category.getDescription(), "Test category description 1");
		category = groupCategoryResponse.getCategories().get(1);
		Assert.assertEquals(category.getName(), "Test category name 2");
		Assert.assertEquals(category.getDescription(), "Test category description 2");
		category = groupCategoryResponse.getCategories().get(2);
		Assert.assertEquals(category.getName(), "Test category name 3");
		Assert.assertEquals(category.getDescription(), "Test category description 3");		
	}

	@Test
	public void testGetCategoryById() throws Exception {
		createCategories();
		Categories categories = API.getCategories();
		GroupCategoryResponse groupCategoryResponse = categories.getCategories();
		for (Category category : groupCategoryResponse.getCategories()) {
			IndividualCategoryResponse individualCategoryResponse = categories.getCategoryById(category.getId());
			Category responseCategory = individualCategoryResponse.getCategory();
			Assert.assertEquals(responseCategory.getId(), category.getId());
			Assert.assertEquals(responseCategory.getName(), category.getName());
			Assert.assertEquals(responseCategory.getDescription(), category.getDescription());
		}
	}

	@Test
	public void testUpdateCategory() throws Exception {
		createCategories();
		Categories categories = API.getCategories();
		GroupCategoryResponse groupCategoryResponse = categories.getCategories();
		Category category = groupCategoryResponse.getCategories().get(0);
		category.setDescription("Test updated description 1");
		IndividualCategoryResponse individualCategoryResponse = categories.updateCategory(category);
		Category responseCategory = individualCategoryResponse.getCategory();
		Assert.assertEquals(responseCategory.getDescription(), category.getDescription());
	}

	private void createCategories() throws Exception {
		Categories categories = API.getCategories();
		Category category = new Category();
		category.setDescription("Test category description 1");
		category.setName("Test category name 1");
		IndividualCategoryResponse individualCategoryResponse = categories.createCategory(category);
		ids.add(individualCategoryResponse.getCategory().getId());
		category = new Category();
		category.setDescription("Test category description 2");
		category.setName("Test category name 2");
		individualCategoryResponse = categories.createCategory(category);
		ids.add(individualCategoryResponse.getCategory().getId());
		category = new Category();
		category.setDescription("Test category description 3");
		category.setName("Test category name 3");
		individualCategoryResponse = categories.createCategory(category);
		ids.add(individualCategoryResponse.getCategory().getId());
	}
}
