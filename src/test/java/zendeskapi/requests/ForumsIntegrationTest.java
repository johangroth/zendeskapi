package zendeskapi.requests;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.categories.Category;
import zendeskapi.models.categories.IndividualCategoryResponse;
import zendeskapi.models.forums.Forum;
import zendeskapi.models.forums.ForumSubscription;
import zendeskapi.models.forums.GroupForumResponse;
import zendeskapi.models.forums.GroupForumSubcriptionResponse;
import zendeskapi.models.forums.IndividualForumResponse;
import zendeskapi.models.forums.IndividualForumSubcriptionResponse;

public class ForumsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);

	private List<Long> forumIds = new ArrayList<Long>();
	private List<Long> forumSubscriptionIds = new ArrayList<Long>();

	@BeforeMethod
	public void createForumsAndForumSubscriptions() throws Exception {
		createForums();
		createForumSubscriptionForAllForums();
	}

	@AfterMethod
	public void deleteForumsAndForumSubscriptions() throws Exception {
		for (Long id : forumSubscriptionIds) {
			API.getForums().deleteForumSubscription(id);
		}
		forumSubscriptionIds.clear();
		for (Long id : forumIds) {
			API.getForums().deleteForum(id);
		}
		forumIds.clear();
	}

	@Test
	public void testCreateForum() throws Exception {
		Forum forum = new Forum();
		forum.setName("Test forum 1");
		IndividualForumResponse individualForumResponse = API.getForums().createForum(forum);
		Forum responseForum = individualForumResponse.getForum();
		Assert.assertEquals(responseForum.getName(), forum.getName());
		forumIds.add(responseForum.getId());
	}

	@Test
	public void testGetForums() throws Exception {
		GroupForumResponse groupForumResponse = API.getForums().getForums();
		Assert.assertTrue(groupForumResponse.getForums().size() > 0);
	}

	@Test
	public void testGetForumById() throws Exception {
		GroupForumResponse groupForumResponse = API.getForums().getForums();
		Forum forum = groupForumResponse.getForums().get(0);
		IndividualForumResponse individualForumResponse = API.getForums().getForumById(forum.getId());
		Assert.assertEquals(individualForumResponse.getForum().getDescription(), forum.getDescription());
		Assert.assertEquals(individualForumResponse.getForum().getId(), forum.getId());
	}

	@Test
	public void testDeleteForum() throws Exception {
		Forum forum = new Forum();
		forum.setName("Test forum 1");
		IndividualForumResponse individualForumResponse = API.getForums().createForum(forum);
		Forum responseForum = individualForumResponse.getForum();
		Assert.assertTrue(API.getForums().deleteForum(responseForum.getId()));
	}

	@Test
	public void testUpdateForum() throws Exception {
		Forum forum = new Forum();
		forum.setName("Test forum 1");
		IndividualForumResponse individualForumResponse = API.getForums().createForum(forum);
		forumIds.add(individualForumResponse.getForum().getId());
		Forum responseForum = individualForumResponse.getForum();
		responseForum.setName("Test update forum 1");
		individualForumResponse = API.getForums().updateForum(responseForum);
		responseForum = individualForumResponse.getForum();
		Assert.assertEquals(responseForum.getName(), "Test update forum 1");
	}

	@Test
	public void testGetForumsByCategory() throws Exception {
		Categories categories = API.getCategories();
		Category category = new Category();
		category.setDescription("Test category description 99");
		category.setName("Test category name 99");
		IndividualCategoryResponse individualCategoryResponse = categories.createCategory(category);
		Category responseCategory = individualCategoryResponse.getCategory();
		
		Forum forum = new Forum();
		forum.setName("Test forum 1");
		forum.setCategoryId(responseCategory.getId());
		IndividualForumResponse individualForumResponse = API.getForums().createForum(forum);
		
		forumIds.add(individualForumResponse.getForum().getId());
		
		GroupForumResponse groupForumResponse = API.getForums().getForumsByCategory(responseCategory.getId());
		List<Forum> forums = groupForumResponse.getForums();
		for (Forum f : forums) {
			Assert.assertEquals(f.getCategoryId(), responseCategory.getId());
		}
		Assert.assertTrue(API.getCategories().deleteCategory(responseCategory.getId()));
	}

	@Test
	public void testGetForumSubscriptions() throws Exception {
		IndividualForumSubcriptionResponse individualForumSubcriptionResponse = getForumSubscription();
		GroupForumSubcriptionResponse forums = API.getForums().getForumSubscriptions();
		Assert.assertTrue(forums.getForumSubscriptions().size() > 0);
		Assert.assertTrue(API.getForums().deleteForumSubscription(individualForumSubcriptionResponse.getForumSubscription().getId()));
	}

	@Test
	public void testGetForumSubscriptionsByForumId() throws Exception {
		GroupForumSubcriptionResponse forums = API.getForums().getForumSubscriptions();
		Assert.assertTrue(forums.getForumSubscriptions().size() > 0);
		GroupForumSubcriptionResponse groupForumSubcriptionResponse = API.getForums().getForumSubscriptionsByForumId(forums.getForumSubscriptions().get(0).getForumId());
		Assert.assertEquals(groupForumSubcriptionResponse.getCount().intValue(), 1);
		Assert.assertEquals(groupForumSubcriptionResponse.getForumSubscriptions().size(), 1);
		Assert.assertEquals(groupForumSubcriptionResponse.getForumSubscriptions().get(0).getForumId(), forums.getForumSubscriptions().get(0).getForumId());
		Assert.assertTrue(API.getForums().deleteForumSubscription(groupForumSubcriptionResponse.getForumSubscriptions().get(0).getId()));
	}

	@Test
	public void testGetForumSubscriptionById() throws Exception {
		IndividualForumSubcriptionResponse individualForumSubcriptionResponse = getForumSubscription();
		IndividualForumSubcriptionResponse response = API.getForums().getForumSubscriptionsById(individualForumSubcriptionResponse.getForumSubscription().getId());
		Assert.assertEquals(response.getForumSubscription().getId(), individualForumSubcriptionResponse.getForumSubscription().getId());
		Assert.assertTrue(API.getForums().deleteForumSubscription(individualForumSubcriptionResponse.getForumSubscription().getId()));
	}

	@Test
	public void testCreateForumSubscription() throws Exception {
		Forum forum = API.getForums().getForums().getForums().get(0);
		ForumSubscription forumSubscription = new ForumSubscription();
		forumSubscription.setForumId(forum.getId());
		forumSubscription.setUserId(USER_ID);
		IndividualForumSubcriptionResponse individualForumSubcriptionResponse = API.getForums().createForumSubscription(forumSubscription);
		Assert.assertTrue(individualForumSubcriptionResponse.getForumSubscription().getId() > 0);
		Assert.assertTrue(API.getForums().deleteForumSubscription(individualForumSubcriptionResponse.getForumSubscription().getId()));
	}

	@Test
	public void testDeleteForumSubscription() throws Exception {
		IndividualForumSubcriptionResponse individualForumSubcriptionResponse = getForumSubscription();
		Assert.assertTrue(API.getForums().deleteForumSubscription(individualForumSubcriptionResponse.getForumSubscription().getId()));
		forumSubscriptionIds.remove(0);
	}
	
	private IndividualForumSubcriptionResponse getForumSubscription() throws ZendeskApiException {
		return API.getForums().getForumSubscriptionsById(forumSubscriptionIds.get(0));
	}

	private void createForumSubscriptionForAllForums() throws ZendeskApiException {
		for (Long forumId : forumIds) {
			Forum forum = API.getForums().getForumById(forumId).getForum();
			ForumSubscription forumSubscription = new ForumSubscription();
			forumSubscription.setForumId(forum.getId());
			forumSubscription.setUserId(USER_ID);
			IndividualForumSubcriptionResponse individualForumSubcriptionResponse = API.getForums().createForumSubscription(forumSubscription);
			Assert.assertTrue(individualForumSubcriptionResponse.getForumSubscription().getId() > 0);
			forumSubscriptionIds.add(individualForumSubcriptionResponse.getForumSubscription().getId());
		}
	}

	private GroupForumResponse createForums() throws ZendeskApiException {
		Forum forum = new Forum();
		for (int i = 0; i < 5; i++) {
			forum.setName("Test forum " + i);
			IndividualForumResponse individualForumResponse = API.getForums().createForum(forum);
			Forum responseForum = individualForumResponse.getForum();
			Assert.assertEquals(responseForum.getName(), forum.getName());
			forumIds.add(responseForum.getId());
		}
		return API.getForums().getForums();
	}
}
