package zendeskapi.requests;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.organizations.Organization;

public class OrganisationIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	
	@Test
	public void testGetOrganisations() throws Exception {
		List<Organization> organizations = API.getOrganizations().getOrganizations().getOrganizations();
		Assert.assertTrue(organizations.size() > 0);
		
		Organization organization = API.getOrganizations().getOrganization(organizations.get(0).getId()).getOrganization();
		Assert.assertTrue(organization.getId() > 0);
	}
	
	@Test
	public void testSearchForOrganisation() throws Exception {
		List<Organization> organizations = API.getOrganizations().getOrganizationsStartingWith(ORGANISATION.substring(0, 3)).getOrganizations();
		Assert.assertTrue(organizations.size() > 0);
		
		Organization organization = new Organization();
		organization.setName("Test organisation 1");
		organization.setExternalId(12431234L);
		Organization createdOrganisation = API.getOrganizations().createOrganization(organization).getOrganization();
		Assert.assertTrue(createdOrganisation.getId() > 0);
		
		List<Organization> searchOrganization = API.getOrganizations().searchForOrganizations("12431234").getOrganizations();
		Assert.assertTrue(searchOrganization.size() > 0);
		
		Assert.assertTrue(API.getOrganizations().deleteOrganization(createdOrganisation.getId()));
	}
	
	@Test
	public void testCreateUpdateAndDeleteOrganisation() throws Exception {
		Organization organization = new Organization();
		organization.setName("Test organisation 1");
		Organization createdOrganisation = API.getOrganizations().createOrganization(organization).getOrganization();
		Assert.assertTrue(createdOrganisation.getId() > 0);
		
		createdOrganisation.setNotes("Test note 1");
		Organization updatedOrganisation = API.getOrganizations().updateOrganization(createdOrganisation).getOrganization();
		Assert.assertEquals(updatedOrganisation.getNotes(), createdOrganisation.getNotes());
		
		Assert.assertTrue(API.getOrganizations().deleteOrganization(createdOrganisation.getId()));
	}
}
