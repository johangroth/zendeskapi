package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;

public class SharingAgreementsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	
	@Test
	public void testGetSharingAgreements() throws Exception {
		Assert.assertNotNull(API.getSharingAgreements().getSharingAgreements().getCount());
	}
}
