package zendeskapi.requests;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.shared.Upload;
import zendeskapi.models.shared.ZendeskFile;

public class AttachmentsIntegrationTest extends ZendeskApiTest {

	@Test
	public void testUploadOfFile() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		Attachments attachments = api.getAttachments();
		ZendeskFile file = new ZendeskFile("src/test/resources/attachments/attachment01.dat");
		file.setFileName("attachment01.dat");
		Upload upload = attachments.uploadAttachment(file);
		Assert.assertNotNull(upload);
		Assert.assertEquals(upload.getAttachments().size(), 1);
		Assert.assertEquals(upload.getAttachments().get(0).getSize().intValue(), 2048);
	}
	
	@Test
	public void testUploadOfThreeFiles() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		Attachments attachments = api.getAttachments();
		ZendeskFile file = new ZendeskFile("src/test/resources/attachments/attachment01.dat");
		file.setFileName("attachment01.dat");		
		List<ZendeskFile> files = new ArrayList<ZendeskFile>();
		files.add(file);
		file = new ZendeskFile("src/test/resources/attachments/attachment02.dat");
		file.setFileName("attachment02.dat");
		files.add(file);
		file = new ZendeskFile("src/test/resources/attachments/attachment03.dat");
		file.setFileName("attachment03.dat");
		files.add(file);
		Upload upload = attachments.uploadAttachments(files);
		Assert.assertEquals(upload.getAttachments().size(), 3);
		Assert.assertEquals(upload.getAttachments().get(0).getFileName(), "attachment01.dat");
		Assert.assertEquals(upload.getAttachments().get(0).getSize().intValue(), 2048);
		Assert.assertEquals(upload.getAttachments().get(1).getFileName(), "attachment02.dat");
		Assert.assertEquals(upload.getAttachments().get(1).getSize().intValue(), 4096);
		Assert.assertEquals(upload.getAttachments().get(2).getFileName(), "attachment03.dat");
		Assert.assertEquals(upload.getAttachments().get(2).getSize().intValue(), 16384);
	}
	
	@Test
	public void testUploadOfLargeFile() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		Attachments attachments = api.getAttachments();
		ZendeskFile file = new ZendeskFile("src/test/resources/attachments/attachment04.dat");
		file.setFileName("attachment04.dat");
		Upload upload = attachments.uploadAttachment(file);
		Assert.assertNotNull(upload);
		Assert.assertEquals(upload.getAttachments().size(), 1);
		Assert.assertEquals(upload.getAttachments().get(0).getSize().intValue(), 16777216);
		
	}

	@Test
	public void testUploadOfTooLargeFile() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		Attachments attachments = api.getAttachments();
		ZendeskFile file = new ZendeskFile("src/test/resources/attachments/attachment05.dat");
		file.setFileName("attachment05.dat");
		try {
			attachments.uploadAttachment(file);			
		} catch (ZendeskApiException e) {
			Assert.assertEquals(e.getMessage(), "zendeskapi.exception.ZendeskApiException: Upload of attachment failed\n422 Unprocessable Entity");
			return;
		}
		Assert.fail("Expected an exception");
	}
}
