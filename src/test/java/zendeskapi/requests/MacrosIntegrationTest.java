package zendeskapi.requests;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.constants.Priority;
import zendeskapi.models.macros.Action;
import zendeskapi.models.macros.Macro;
import zendeskapi.models.tickets.Ticket;

public class MacrosIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	
	@Test
	public void testGetActiveMacros() throws Exception {
		Assert.assertTrue(API.getMacros().getActiveMacros().getMacros().size() > 0);
	}
	
	@Test
	public void testGetAllMacros() throws Exception {
		Assert.assertTrue(API.getMacros().getAllMacros().getMacros().size() > 0);
	}
	
	@Test
	public void testGetMacroById() throws Exception {
		Macro macro = API.getMacros().getAllMacros().getMacros().get(0);
		Assert.assertTrue(API.getMacros().getMacroById(macro.getId()).getMacro().getId() > 0);
	}
	
	@Test
	public void testCreateUpdateApplyAndDeleteMacro() throws Exception {
		Macro macro = new Macro();
		macro.setTitle("Test Macro 1");
		List<Action> actions = new ArrayList<Action>();
		Action action = new Action();
		action.setField("status");
		action.setValue("open");
		actions.add(action);
		macro.setActions(actions);
		
		// Create 
		Macro responseMacro = API.getMacros().createMacro(macro).getMacro();
		Assert.assertTrue(responseMacro.getId() > 0);
		
		// Update
		responseMacro.setTitle("Test Macro 2");
		Macro updateMacro = API.getMacros().updateMacro(responseMacro).getMacro();
		Assert.assertEquals(updateMacro.getId(), responseMacro.getId());
		
		// Apply
		Ticket ticket = new Ticket();
		ticket.setSubject("Macro test ticket");
		ticket.setDescription("Testing macros");
		ticket.setPriority(Priority.NORMAL);
		Ticket responseTicket = API.getTickets().createTicket(ticket).getTicket();
		Ticket applyTicket = API.getMacros().applyMacroToTicket(responseTicket.getId(), responseMacro.getId()).getResult().getTicket();
		Assert.assertEquals(applyTicket.getStatus(), "open");
		
		// Delete
		Assert.assertTrue(API.getTickets().delete(responseTicket.getId()));
		Assert.assertTrue(API.getMacros().deleteMacro(responseMacro.getId()));
	}
}
