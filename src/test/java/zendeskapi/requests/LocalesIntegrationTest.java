package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.locales.Locale;

public class LocalesIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	
	@Test
	public void testGetAllLocales() throws Exception {
		Assert.assertTrue(API.getLocales().getAllLocales().getLocales().size() > 0);
	}
	
	@Test
	public void testGetCurrenLocale() throws Exception {
		Assert.assertTrue(API.getLocales().getCurrentLocale().getLocale().getId() > 0);
	}
	
	@Test
	public void testLocaleById() throws Exception {
		Locale locale = API.getLocales().getAllLocales().getLocales().get(0);
		Assert.assertTrue(API.getLocales().getLocaleById(locale.getId()).getLocale().getId() > 0);
	}
	
	@Test
	public void testGetLocalesForAgents() throws Exception {
		Assert.assertTrue(API.getLocales().getLocalesForAgents().getLocales().size() > 0);
	}
}
