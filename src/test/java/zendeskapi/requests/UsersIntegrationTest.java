package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.users.GroupUserResponse;
import zendeskapi.models.users.User;

public class UsersIntegrationTest extends ZendeskApiTest {
	@Test
	public void testGetAllUsers() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		GroupUserResponse users = api.getUsers().getAllUsers();
		Assert.assertTrue(users.getCount() > 0);
	}

	@Test
	public void testGetOneUser() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		GroupUserResponse users = api.getUsers().getAllUsers();
		long id = users.getUsers().get(0).getId();
		User user = api.getUsers().getUser(id).getUser();
		Assert.assertEquals(user.getEmail(), "johan.groth@linuxgrotto.org.uk");
	}

	@Test
	public void testGetCurrentUser() throws Exception {
		ZendeskApi api = new ZendeskApi(URL, USER, PASSWORD);
		User user = api.getUsers().getCurrentUser().getUser();
		Assert.assertEquals(user.getEmail(), USER);
	}
}
