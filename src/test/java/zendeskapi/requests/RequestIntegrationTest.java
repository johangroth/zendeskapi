package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.requests.GroupCommentResponse;
import zendeskapi.models.requests.IndividualCommentResponse;
import zendeskapi.models.requests.IndividualRequestResponse;
import zendeskapi.models.requests.Request;
import zendeskapi.models.tickets.Comment;

public class RequestIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	
	@Test
	public void testGetAllRequests() throws Exception {
		Assert.assertTrue(API.getRequests().getAllRequests().getCount() > 0);
	}
	
	@Test
	public void testCreateUpdateAndDeleteRequest() throws Exception {
		Request request = new Request();
		Comment comment = new Comment();
		comment.setBody("Test comment 1");
		request.setSubject("Test request 1");
		request.setComment(comment);
		IndividualRequestResponse irr = API.getRequests().createRequest(request);
		Assert.assertTrue(irr.getRequest().getId() > 0);
		
		IndividualRequestResponse irrById = API.getRequests().getRequestById(irr.getRequest().getId());
		Assert.assertEquals(irrById.getRequest().getId(), irr.getRequest().getId());
		
		irrById.getRequest().setSubject("New Subject");
		Comment newComment = new Comment();
		newComment.setBody("something more to say");
		irrById.getRequest().setComment(newComment);
		
		API.getRequests().updateRequest(irrById.getRequest());
		GroupCommentResponse gcr = API.getRequests().getRequestCommentsById(irr.getRequest().getId());
		Assert.assertEquals(gcr.getComments().get(gcr.getComments().size()-1).getBody().replace("\n", ""), "something more to say");
		
		IndividualCommentResponse icr = API.getRequests().getSpecificRequestComment(irr.getRequest().getId(), gcr.getComments().get(gcr.getComments().size()-1).getId());
		Assert.assertTrue(icr.getComment().getId().longValue() == gcr.getComments().get(gcr.getComments().size()-1).getId().longValue());
		
		Assert.assertTrue(API.getTickets().delete(irrById.getRequest().getId()));
	}
}
