package zendeskapi.requests;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.tags.Tag;

public class TagsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	
	@Test
	public void testGetTags() throws Exception {
		Assert.assertTrue(API.getTags().getTags().getCount() > 0);
	}
	
	@Test
	public void testAutocompleteTags() throws Exception {
		List<Tag> tags = API.getTags().getTags().getTags();
		List<String> auto = API.getTags().autocompleteTags(tags.get(0).getName().substring(0, 2)).getTags();
		Assert.assertTrue(auto.size() > 0);
	}
}
