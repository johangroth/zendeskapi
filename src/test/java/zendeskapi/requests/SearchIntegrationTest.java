package zendeskapi.requests;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.search.SearchResults;
import zendeskapi.models.tickets.Ticket;
import zendeskapi.models.topics.Topic;
import zendeskapi.models.users.User;

public class SearchIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);

	@Test(enabled = false)
	public void testSearch() throws Exception {
		SearchResults searchResults = API.getSearch().searchFor(USER);
		List<User> users = searchResults.getEntitiesFromSearch(User.class);
		Assert.assertTrue(users.size() > 0);

		List<Ticket> tickets = searchResults.getEntitiesFromSearch(Ticket.class);
		Assert.assertTrue(tickets.size() > 0);

		List<Topic> topics = searchResults.getEntitiesFromSearch(Topic.class);
		Assert.assertTrue(topics.size() > 0);
	}
}
