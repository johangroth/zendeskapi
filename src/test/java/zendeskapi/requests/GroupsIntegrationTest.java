package zendeskapi.requests;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.groups.Group;
import zendeskapi.models.groups.GroupMembership;
import zendeskapi.models.groups.IndividualGroupMembershipResponse;
import zendeskapi.models.groups.IndividualGroupResponse;
import zendeskapi.models.groups.MultipleGroupMembershipResponse;
import zendeskapi.models.groups.MultipleGroupResponse;
import zendeskapi.models.users.User;

public class GroupsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	private final List<Long> groupIds = new ArrayList<Long>();
	private static final Logger LOG = Logger.getLogger(GroupsIntegrationTest.class.getName());

	@AfterMethod
	public void deleteGroups() throws Exception {
		for (Long id : groupIds) {
			API.getGroups().deleteGroup(id);
		}
		groupIds.clear();
	}

	@BeforeMethod
	public void deleteTestUsers() throws Exception {
		List<User> users = API.getUsers().getAllUsers().getUsers();
		for (User user : users) {
			if (user.getName().contains("Test user")) {
				API.getUsers().deleteUser(user.getId());
			}
		}
		List<GroupMembership> groupMemberShips = API.getGroups().getGroupMemberships().getGroupMemberships();
		for (GroupMembership groupMembership : groupMemberShips) {
			LOG.info("Delete " + API.getGroups().deleteGroupMembership(groupMembership.getId()));
		}
	}

	@Test
	public void testGetGroups() throws Exception {
		LOG.info("Get groups");
		MultipleGroupResponse mgr = API.getGroups().getGroups();
		List<Group> groups = mgr.getGroups();
		Assert.assertEquals(mgr.getCount().intValue(), 1);
		Assert.assertEquals(groups.size(), 1);
		Assert.assertEquals(groups.get(0).getName(), "Support");
	}

	@Test
	public void testGetAssignableGroups() throws Exception {
		LOG.info("Get assignable groups");
		MultipleGroupResponse mgr = API.getGroups().getAssignableGroups();
		List<Group> groups = mgr.getGroups();
		Assert.assertEquals(mgr.getCount().intValue(), 1);
		Assert.assertEquals(groups.size(), 1);
		Assert.assertEquals(groups.get(0).getName(), "Support");
	}

	@Test
	public void testGetGroupById() throws Exception {
		LOG.info("Get group by id");
		MultipleGroupResponse mgr = API.getGroups().getGroups();
		List<Group> groups = mgr.getGroups();
		Group groupById = API.getGroups().getGroupById(groups.get(0).getId()).getGroup();
		Assert.assertEquals(groupById.getId(), groups.get(0).getId());
	}

	@Test
	public void testCreateGroup() throws Exception {
		LOG.info("Create group");
		IndividualGroupResponse igr = API.getGroups().createGroup("Test Group 1");
		Assert.assertEquals(igr.getGroup().getName(), "Test Group 1");
		groupIds.add(igr.getGroup().getId());
	}

	@Test
	public void testUpdateGroup() throws Exception {
		LOG.info("Update Group");
		IndividualGroupResponse igr = API.getGroups().createGroup("Test Group 1");
		Assert.assertEquals(igr.getGroup().getName(), "Test Group 1");
		groupIds.add(igr.getGroup().getId());
		igr.getGroup().setName("Test Group 1 updated");
		IndividualGroupResponse updated = API.getGroups().updateGroup(igr.getGroup());
		Assert.assertEquals(updated.getGroup().getName(), "Test Group 1 updated");
		Assert.assertEquals(updated.getGroup().getId(), igr.getGroup().getId());
	}

	@Test
	public void testDeleteGroup() throws Exception {
		LOG.info("Delete group");
		IndividualGroupResponse igr = API.getGroups().createGroup("Test Group 1");
		Assert.assertEquals(igr.getGroup().getName(), "Test Group 1");
		Assert.assertTrue(API.getGroups().deleteGroup(igr.getGroup().getId()));
	}

	@Test(enabled = false)
	public void testGetGroupMemeberships() throws Exception {
		LOG.info("Get group memberships");
		MultipleGroupMembershipResponse mgmr = API.getGroups().getGroupMemberships();
		Assert.assertEquals(mgmr.getCount().intValue(), 3);
		Assert.assertEquals(mgmr.getGroupMemberships().size(), 3);
	}

	@Test
	public void testGetGroupMembershipsByUser() throws Exception {
		LOG.info("Get group membership by user");
		MultipleGroupMembershipResponse mgmr = API.getGroups().getGroupMembershipsByUser(USER_ID);
		Assert.assertEquals(mgmr.getCount().intValue(), 1);
		Assert.assertEquals(mgmr.getGroupMemberships().size(), 1);
	}

	@Test(enabled = false)
	public void testGetGroupMembershipsByGroup() throws Exception {
		LOG.info("Get group membership by user");
		long groupId = API.getGroups().getGroups().getGroups().get(0).getId();
		MultipleGroupMembershipResponse mgmr = API.getGroups().getGroupMembershipsByGroup(groupId);
		Assert.assertEquals(mgmr.getCount().intValue(), 3);
		Assert.assertEquals(mgmr.getGroupMemberships().size(), 3);
	}

	@Test
	public void testGetAssignableGroupMembemships() throws Exception {
		LOG.info("Get assignable group memberships");
		MultipleGroupMembershipResponse mgmr = API.getGroups().getAssignableGroupMemberships();
		Assert.assertEquals(mgmr.getCount().intValue(), 1);
		Assert.assertEquals(mgmr.getGroupMemberships().size(), 1);
	}

	@Test(enabled = false)
	public void testGetAssignableGroupMembershipsByGroup() throws Exception {
		LOG.info("Get assignable group memberships by group");
		long groupId = API.getGroups().getGroups().getGroups().get(0).getId();
		MultipleGroupMembershipResponse mgmr = API.getGroups().getAssignableGroupMembershipsByGroup(groupId);
		Assert.assertEquals(mgmr.getCount().intValue(), 1);
		Assert.assertEquals(mgmr.getGroupMemberships().size(), 1);
	}

	@Test
	public void testGetGroupMembershipsByMembershipId() throws Exception {
		LOG.info("Get group membership by membership id");
		MultipleGroupMembershipResponse mgmr = API.getGroups().getGroupMemberships();
		IndividualGroupMembershipResponse igmr = API.getGroups().getGroupMembershipsByMembershipId(mgmr.getGroupMemberships().get(0).getId());
		Assert.assertEquals(igmr.getGroupMembership().getId(), mgmr.getGroupMemberships().get(0).getId());
	}

	@Test
	public void testGetGroupMembershipsByUserAndMembershipId() throws Exception {
		LOG.info("Get group membership by user and membership id");
		MultipleGroupMembershipResponse mgmr = API.getGroups().getGroupMemberships();
		IndividualGroupMembershipResponse igmr = API.getGroups().getGroupMembershipsByMembershipId(mgmr.getGroupMemberships().get(0).getId());
		IndividualGroupMembershipResponse igmr2 = API.getGroups().getGroupMembershipsByUserAndMembershipId(igmr.getGroupMembership().getUserId(),
				mgmr.getGroupMemberships().get(0).getId());
		Assert.assertEquals(igmr2.getGroupMembership().getUserId(), igmr.getGroupMembership().getUserId());
	}

	@Test
	public void testCreateUpdateAndDeleteGroupMembership() throws Exception {
		LOG.info("Create, set default and delete group membership");
		Group group = API.getGroups().createGroup("Test group 1").getGroup();
		groupIds.add(group.getId());
		User user = new User();
		user.setName("Test user 1");
		user.setEmail("test.user1@linxugrotto.org.uk");
		user.setRole("agent");
		User createdUser = API.getUsers().createUser(user).getUser();
		GroupMembership groupMembership = new GroupMembership();
		groupMembership.setUserId(createdUser.getId());
		groupMembership.setGroupId(group.getId());

		// Create
		IndividualGroupMembershipResponse igmr = API.getGroups().createGroupMembership(groupMembership);
		Assert.assertTrue(igmr.getGroupMembership().getId() > 0);

		// Update
		MultipleGroupMembershipResponse mgmr = API.getGroups().setGroupMembershipAsDefault(createdUser.getId(), igmr.getGroupMembership().getId());
		Assert.assertEquals(mgmr.getGroupMemberships().size(), 2);
		Assert.assertEquals(mgmr.getGroupMemberships().get(1).getId(), igmr.getGroupMembership().getId());
		Assert.assertTrue(mgmr.getGroupMemberships().get(1).getDefaultGroup());

		// Delete
		Assert.assertTrue(API.getGroups().deleteGroupMembership(igmr.getGroupMembership().getId()));
		Assert.assertTrue(API.getUsers().deleteUser(createdUser.getId()));
		Assert.assertTrue(API.getGroups().deleteGroup(group.getId()));
	}
}
