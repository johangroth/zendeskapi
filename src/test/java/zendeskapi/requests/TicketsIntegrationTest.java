package zendeskapi.requests;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.models.constants.Priority;
import zendeskapi.models.tickets.Comment;
import zendeskapi.models.tickets.CustomField;
import zendeskapi.models.tickets.IndividualTicketResponse;
import zendeskapi.models.tickets.Ticket;

public class TicketsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	private static final Logger LOG = Logger.getLogger(TicketsIntegrationTest.class);

	@Test
	public void testGetTickets() throws Exception {
		List<Ticket> tickets = API.getTickets().getAllTickets().getTickets();
		Assert.assertTrue(tickets.size() > 0);
		List<Ticket> ticketsByUser = API.getTickets().getTicketsByUserID(tickets.get(0).getRequesterId()).getTickets();
		LOG.info("Ticket id is " + ticketsByUser.get(0).getId());
		Assert.assertTrue(ticketsByUser.size() > 0);
	}

	@Test
	public void testGetTicketById() throws Exception {
		Ticket ticket = API.getTickets().getTicket(ID).getTicket();
		Assert.assertNotNull(ticket);
		Assert.assertEquals(ticket.getId(), ID);
	}

	@Test
	public void testGetTicketByOrganisationId() throws Exception {
		List<Ticket> tickets = API.getTickets().getTicketsByOrganizationID(ORGANISATION_ID).getTickets();
		Assert.assertTrue(tickets.size() > 0);
	}

	@Test
	public void testGetMultipleTickets() throws Exception {
		List<Long> ids = new ArrayList<Long>();
		ids.add(1L);
		List<Ticket> tickets = API.getTickets().getMultipleTickets(ids).getTickets();
		Assert.assertNotNull(tickets);
		Assert.assertTrue(tickets.size() > 0);
	}

	@Test
	public void testCreateUpdateAndDeleteTicket() throws Exception {
		Ticket ticket = new Ticket();
		ticket.setSubject("My printer is on fire");
		ticket.setDescription("Please send support");
		ticket.setPriority(Priority.high);
		List<CustomField> customFields = new ArrayList<CustomField>();
		CustomField customField = new CustomField();
		customField.setId(CUSTOM_FIELD_ID);
		customField.setValue("testing");
		customFields.add(customField);
		ticket.setCustomFields(customFields);
		Ticket createdTicket = API.getTickets().createTicket(ticket).getTicket();
		Assert.assertNotNull(createdTicket);
		Assert.assertTrue(createdTicket.getId() > 0);

		ticket = new Ticket();
		ticket.setId(createdTicket.getId());
//		ticket.setStatus(TicketStatus.open);
//		ticket.setAssigneeId(USER_ID);
//		List<String> collaboratorEmails = new ArrayList<String>();
//		collaboratorEmails.add(COLLABORATOR_EMAIL);
//		ticket.setCollaboratorEmails(collaboratorEmails);
		String body = "Got it, thanks";
		Comment newComment = new Comment();
		newComment.setBody(body);
		newComment.setPublicComment(true);
		IndividualTicketResponse res = API.getTickets().updateTicket(ticket, newComment);
		Ticket updatedTicket = res.getTicket();
		Assert.assertNotNull(updatedTicket);
		Assert.assertEquals(res.getAudit().getEvents().get(0).getBody(), body);

		//Delete
		Assert.assertTrue(API.getTickets().delete(createdTicket.getId()));
	}
}
