package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;

public class CustomAgentRolesIntegrationTest extends ZendeskApiTest {

	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);

	/**
	 * This test will only succeed if the Zendesk account is of Enterprise type.
	 * @throws Exception
	 */
	@Test(enabled = false)
	public void testCustomAgentRoles() throws Exception {
		CustomAgentRoles customAgentRoles = API.getCustomAgentRoles();
		Assert.assertTrue(customAgentRoles.getCustomRoles().getCustomRoleCollection().size() > 0);
	}
}
