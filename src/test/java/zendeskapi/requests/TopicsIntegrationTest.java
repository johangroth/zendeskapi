package zendeskapi.requests;

import org.testng.Assert;
import org.testng.annotations.Test;

import zendeskapi.ZendeskApi;
import zendeskapi.ZendeskApiTest;
import zendeskapi.exception.ZendeskApiException;
import zendeskapi.models.topics.GroupTopicResponse;
import zendeskapi.models.topics.IndividualTopicResponse;

public class TopicsIntegrationTest extends ZendeskApiTest {
	private static final ZendeskApi API = new ZendeskApi(URL, USER, PASSWORD);
	private long forumId;

	public TopicsIntegrationTest() {
		try {
			this.forumId = API.getForums().getForums().getForums().get(0).getId();
		} catch (ZendeskApiException e) {
			this.forumId = 0;
		}
	}

	@Test(enabled = false)
	public void testGetTopics() throws Exception {
		GroupTopicResponse gtr = API.getTopics().getTopics();
		Assert.assertTrue(gtr.getCount().intValue() > 0);

		IndividualTopicResponse itr = API.getTopics().getTopicById(gtr.getTopics().get(0).getId());
		Assert.assertEquals(itr.getTopic().getId(), gtr.getTopics().get(0).getId());

		GroupTopicResponse tbf = API.getTopics().getTopicsByForum(gtr.getTopics().get(0).getForumId());
		Assert.assertEquals(tbf.getTopics().get(0).getForumId(), gtr.getTopics().get(0).getForumId());

		GroupTopicResponse tbu = API.getTopics().getTopicsByUser(USER_ID);
		Assert.assertTrue(tbu.getCount().intValue() > 0);
	}
}
